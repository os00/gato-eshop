// let mapElement;
//
// // Trigger event when new page is load by turbolinks
// document.addEventListener("turbolinks:load", () => {
//   // Initialize map when home page is loaded
//   mapElement = document.getElementById('map');
//   if (mapElement) {
//     initMap(49.920430, 14.662644);
//   }
//   // Hide or show to-top-btn
//   toTopOnScroll();
//
//   // Catch click on all '#' link tags in order to scroll smoothly
//   const pageTopAnchor = document.getElementById('page-top');
//   if (pageTopAnchor) {
//     document.querySelectorAll('a[href*="#"]').forEach(anchor => {
//       anchor.addEventListener('click', event => { smoothScrollonClick(event, anchor.getAttribute('href')) });
//     });
//   }
// });
//
//
// const smoothScrollonClick = (event, hrefTarget) => {
//   event.preventDefault();
//   const selector = hrefTarget.substring(hrefTarget.indexOf('#'));
//   document.querySelector(selector).scrollIntoView({ behavior: 'smooth' });
// }
//
// // Hide or gradually show to-top-btn
// const toTopOnScroll = () => {
//   const homeToTop = document.getElementById('home-to-top');
//
//   if (homeToTop) {
//     if (window.scrollY < (window.innerHeight * 0.3)) {
//       homeToTop.classList.add("invisible");
//     } else if ((window.innerHeight * 0.3 < window.scrollY) &&
//                (window.scrollY < window.innerHeight * 0.7)) {
//       homeToTop.classList.remove("invisible");
//       homeToTop.style.opacity = (window.scrollY - window.innerHeight * 0.3) /
//                                 (window.innerHeight * 0.7 - window.innerHeight * 0.3)
//     } else {
//       homeToTop.classList.remove("invisible");
//       homeToTop.style.opacity = 1.0;
//     }
//   }
// }
//
// window.addEventListener('scroll', toTopOnScroll);
//
// const initMap = (lat, lng) => {
//   const myCoordsCenter = new google.maps.LatLng(lat, lng);
//   const myCoordsMarker = new google.maps.LatLng(lat, lng);
//   const mapOptions = {
//       center: myCoordsCenter,
//       zoom: 13,
//       mapTypeId: 'hybrid'
//     };
//   const map = new google.maps.Map(mapElement, mapOptions);
//
//   const Popup = createPopupClass();
//   const popup = new Popup(
//       myCoordsMarker,
//       document.getElementById('map-popup'));
//   popup.setMap(map);
// }
//
// /**
//  * Returns the Popup class.
//  *
//  * Unfortunately, the Popup class can only be defined after
//  * google.maps.OverlayView is defined, when the Maps API is loaded.
//  * This function should be called by initMap.
//  */
// function createPopupClass() {
//   /**
//    * A customized popup on the map.
//    * @param {!google.maps.LatLng} position
//    * @param {!Element} content The bubble div.
//    * @constructor
//    * @extends {google.maps.OverlayView}
//    */
//   function Popup(position, content) {
//     this.position = position;
//
//     content.classList.add('popup-bubble');
//
//     // This zero-height div is positioned at the bottom of the bubble.
//     var bubbleAnchor = document.createElement('div');
//     bubbleAnchor.classList.add('popup-bubble-anchor');
//     bubbleAnchor.appendChild(content);
//
//     // This zero-height div is positioned at the bottom of the tip.
//     this.containerDiv = document.createElement('div');
//     this.containerDiv.classList.add('popup-container');
//     this.containerDiv.appendChild(bubbleAnchor);
//
//     // Optionally stop clicks, etc., from bubbling up to the map.
//     google.maps.OverlayView.preventMapHitsAndGesturesFrom(this.containerDiv);
//   }
//   // ES5 magic to extend google.maps.OverlayView.
//   Popup.prototype = Object.create(google.maps.OverlayView.prototype);
//
//   /** Called when the popup is added to the map. */
//   Popup.prototype.onAdd = function() {
//     this.getPanes().floatPane.appendChild(this.containerDiv);
//   };
//
//   /** Called when the popup is removed from the map. */
//   Popup.prototype.onRemove = function() {
//     if (this.containerDiv.parentElement) {
//       this.containerDiv.parentElement.removeChild(this.containerDiv);
//     }
//   };
//
//   /** Called each frame when the popup needs to draw itself. */
//   Popup.prototype.draw = function() {
//     var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
//
//     // Hide the popup when it is far out of view.
//     var display =
//         Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
//         'block' :
//         'none';
//
//     if (display === 'block') {
//       this.containerDiv.style.left = divPosition.x + 'px';
//       this.containerDiv.style.top = divPosition.y + 'px';
//     }
//     if (this.containerDiv.style.display !== display) {
//       this.containerDiv.style.display = display;
//     }
//   };
//
//   return Popup;
// }
