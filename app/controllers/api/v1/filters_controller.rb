class Api::V1::ProductsController < ActionController::Base
  def index
    filter_hash = prepare_filter_hash(params)
    products = filtered_products(filter_hash)
    filters = prepare_filters(filter_hash, products.count('id'))
    render json: { products: products.as_json(current_user: current_user),
                   filters: filters }
  end

  private

end
