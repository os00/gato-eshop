class Api::V1::ProductsController < ActionController::Base
  def index
    filter_hash = prepare_filter_hash(params)
    products = filtered_products(filter_hash)
    filters = prepare_filters(filter_hash, products.count('id'))
    render json: { products: products.as_json(current_user: current_user),
                   filters: filters }
  end

  private

  def prepare_filter_hash(params)
    where_string = '(ag_params @> ARRAY[?]::text[])'
    filter_string = {}
    filter_values = {}

    # For every param prepare condition
    params.each do |param, value|
      next unless param.start_with?('_')

      pc = param[3..].to_sym
      if filter_string[pc].nil?
        filter_string[pc] = []
        filter_values[pc] = []
      end

      filter_string[pc] << where_string
      filter_values[pc] << "#{pc}=#{value}"
    end

    { string: filter_string, values: filter_values }
  end

  def filtered_products(filter_hash)
    # Flatten the filter_hash in order to use it as a condition in SQL command
    filter_string = filter_hash[:string].map { |_, v| "(#{v.join(' OR ')})" }.join(' AND ')
    filter_values = filter_hash[:values].map { |_, v| v.flatten }.flatten

    # Query database and get the filtered products
    AggregateParams
      .select('id, name, brief_description, description')
      .where(filter_string, *filter_values)
  end

  # Prepare number of products for every filter option
  def prepare_filters(filter_hash, products_count)
    # Get all the filterable params
    product_parameters =
      Parameter
      .select(:name, :filter_order)
      .joins(:products)
      .where(is_filterable: true)
      .group(:name, :filter_order)
      .order(:filter_order)
      .map do |product_parameter|
        # For every filterable parameter, get all used values
        parameter_values =
          ProductParameter
          .select(:value)
          .joins(:parameter)
          .where(parameters: { name: product_parameter.name })
          .group(:value)
          .map do |parameter_value|
            { value_name: parameter_value.value,
              filter_num: filter_number(product_parameter.name, parameter_value.value, filter_hash, products_count) }
          end

        { name: product_parameter.name, values: parameter_values }
      end

    product_parameters
  end

  # Get number of products for a specific parameter value
  def filter_number(current_param, current_param_value, current_filter_hash, products_count)
    filter_hash = current_filter_hash.clone
    plus = true

    if filter_hash[:string][current_param.to_sym].nil?
      filter_hash[:string][current_param.to_sym] = []
      filter_hash[:values][current_param.to_sym] = []
      plus = false
    end

    return -1 if filter_hash[:values][current_param.to_sym].include?("#{current_param}=#{current_param_value}")

    filter_hash[:string][current_param.to_sym] << '(ag_params @> ARRAY[?]::text[])'
    filter_hash[:values][current_param.to_sym] << "#{current_param}=#{current_param_value}"

    # Flatten the filter_hash in order to use it as a condition in SQL command
    filter_string = filter_hash[:string].map { |_, v| "(#{v.join(' OR ')})" }.join(' AND ')
    filter_values = filter_hash[:values].map { |_, v| v.flatten }.flatten

    if filter_hash[:string][current_param.to_sym].count == 1
      filter_hash[:string].delete(current_param.to_sym)
      filter_hash[:values].delete(current_param.to_sym)
    else
      filter_hash[:string][current_param.to_sym].pop
      filter_hash[:values][current_param.to_sym].pop
    end

    # Query database and get the number of products
    count = AggregateParams
            .select('id')
            .where(filter_string, *filter_values)
            .count

    plus ? "+ #{count - products_count}" : count.to_s
  end
end
