class HomeController < ApplicationController
  before_action :authenticate_user! # , :except => [:index]

  def index
    @contact = Contact.new
    @contact.messages.build
  end

  def create
    puts '***** PARAMS *****'
    p contact_params

    params = contact_params

    @contact = if Contact.exists?(email: params[:email])
                 update_contact(params)
               else
                 Contact.new(params)
               end
    p '@contact', @contact
    if @contact.save
      ContactFormMailer.new_contact_message(@contact.messages.last).deliver_now
      redirect_to(root_path, notice: 'Zpráva odeslána')
    else
      render(:index)
    end
  end

  private

  def contact_params
    params.require(:contact).permit(
      :first_name, :last_name, :email, :phone,
      messages_attributes: [:subject, :message]
    )
  end

  def update_contact(params)
    contact = Contact.find_by(email: params[:email])
    p 'contact1:', contact
    if contact.user
      if contact.phone.nil?
        contact.assign_attributes(email: params[:email],
                                  phone: params[:phone],
                                  messages_attributes: params[:messages_attributes])
      else
        contact.assign_attributes(email: params[:email],
                                  messages_attributes: params[:messages_attributes])
        p 'contact2:', contact
      end
    else
      contact.assign_attributes(params)
    end

    return contact
  end
end
