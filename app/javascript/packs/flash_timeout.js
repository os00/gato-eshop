
if ($('#flash-notice')) {
  setTimeout(() => {
    $('#flash-notice').alert('close');
  }, 5000);
}

if ($('#flash-warning')) {
  setTimeout(() => {
    $('#flash-warning').alert('close');
  }, 5000);
}
