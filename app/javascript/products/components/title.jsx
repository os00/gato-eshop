import React from 'react';
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';

const Title = (props) => {

  return (
    <StyledTitleBox>
      <h1>Profily</h1>
    </StyledTitleBox>
  );
}

export default Title;

// *** Styles ***
const StyledTitleBox = styled.div`
  padding: 30px 0 0 24px;
`;
