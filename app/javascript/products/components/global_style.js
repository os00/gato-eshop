import { createGlobalStyle } from 'styled-components';

export const globalTheme = {
  poppinsFont: "\'Poppins\', \'sans-serif\'",
  loraFont: "\'Lora\', \'serif\'",
  blackgrey: '#333333',
  greyBB: '#BBBBBB',
  grey66: '#666666',
  grey77: '#777777',
  greyE5: '#E5E5E5',
  grey99: '#999999',
  yellow: '#F8C335',
  bluegreen: '#207790',
  lightBluegreen: '#4F9CAF',
  backgroundBlue: '#DAE9FC',
  backgroundGrey: '#F4F8FC',
  red: '#FD1015'
}

export const GlobalStyle = createGlobalStyle`

  h1 {
    font-family: ${props => props.theme.poppinsFont};
    font-size: 2.8125em;
    font-weight: 400;
    color: ${props => props.theme.bluegreen};

    margin: 0;
  }

  h2 {
    font-family: ${props => props.theme.poppinsFont};
    font-size: 1.25em;
    font-weight: 600;
    color: ${props => props.theme.blackgrey};

    margin: 0;
  }

  h3 {
    font-family: ${props => props.theme.poppinsFont};
    font-size: 0.9375em;
    font-weight: 400;
    color: ${props => props.theme.blackgrey};

    margin: 0;
  }

  h6 {
    font-family: ${props => props.theme.poppinsFont};
    font-size: 0.8125em;
    font-weight: 400;
    color: ${props => props.theme.blackgrey};

    margin: 0;
  }

  p {
    font-family: ${props => props.theme.loraFont};
    font-size: 0.75em;
    font-weight: 400;
    color: ${props => props.theme.grey77};

    margin: 0;
  }

  input {
    border: 1px solid ${props => props.theme.greyE5};
    box-shadow: none;
    border-radius: 3px;
    background-color: white;
    height: 2.6em;
    width: 100%;

    font-family: ${props => props.theme.loraFont};
    font-weight: 400;
    font-size: 1em;
    color: ${props => props.theme.blackgrey};

    transition: box-shadow 0.4s ease;

    &:focus {
      border: 1px solid ${props => props.theme.yellow};
      box-shadow: 0 0 0 1px ${props => props.theme.yellow};
      color: ${props => props.theme.grey66};
      outline: none;
    }
  }

  a {
    &:hover {
      text-decoration: none;
    }
  }
`;
