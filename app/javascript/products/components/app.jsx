import React from 'react';
//import { Link } from 'react-router-dom';
//import { useSelector } from 'react-redux'
import styled, { ThemeProvider } from 'styled-components';
import { useMediaQuery } from 'react-responsive';

import { GlobalStyle, globalTheme } from './global_style';
import Title from './title';
import Menu from '../containers/menu';
import Products from '../containers/products';

const App = (props) => {
  const fullWidth = useMediaQuery({ maxWidth: 1370 });
  const desktopSideMenu = useMediaQuery({ minWidth: 451});
  const mobileSideMenu = useMediaQuery({ maxWidth: 450});

  console.log(fullWidth);

  return (
    <ThemeProvider theme={globalTheme}>
      <GlobalStyle />
      <StyledProductsContainer>
        <StyledAppBox fullWidth={fullWidth} desktopSideMenu={desktopSideMenu}>
          <StyledEmpty></StyledEmpty>
          <StyledTitle>
            <Title />
          </StyledTitle>
          {desktopSideMenu && <StyledMenu>
            <Menu />
          </StyledMenu>}
          <StyledProducts>
            <Products />
          </StyledProducts>
        </StyledAppBox>
        {mobileSideMenu && <Menu />}
      </StyledProductsContainer>
    </ThemeProvider>
  );
}

export default App;

// *** Styles ***
const StyledProductsContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 100vw;
`;

const StyledAppBox = styled.div`
  background-color: white;
  display: grid;
  grid-template-columns: ${props => {
    if (props.desktopSideMenu) {
      return "minmax(150px, 250px) minmax(260px, 100%)"
    } else {
      return "minmax(0px, 150px) minmax(260px, 100%)"
    }
  }};
  grid-template-rows: auto minmax(100vh auto);

  gap: 40px;

  width: ${props => props.fullWidth ? '100%' : '80%'};
  max-width: 1360px;
  margin: 3rem auto 0 auto;

  grid-template-areas:
    "empty title"
    "menu products";
`;

const StyledEmpty = styled.div`
    grid-area: empty;
`;
const StyledTitle = styled.div`
  grid-area: title;
`;
const StyledMenu = styled.div`
  grid-area: menu;
`;
const StyledProducts = styled.div`
  grid-area: products;
`;


