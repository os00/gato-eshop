import React, { useState } from 'react';
//import { Link } from 'react-router-dom';
//import { useSelector } from 'react-redux'
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';

import FilterItem from './filter_item';

const Filter = (props) => {
  const { filter } = props;
  const [isVisible, setIsVisible] = useState(true);

  return (
    <StyledFilterBox key={filter.name}>
      <StyledTitleBox>
        <StyledFilterButton onClick={() => isVisible ? setIsVisible(false) : setIsVisible(true)}>
          <h6>{isVisible ? '-' : '+'}</h6>
        </StyledFilterButton>
        <h6>{filter.name.toUpperCase()}</h6>
      </StyledTitleBox>
      <StyledValuesUl isVisible={isVisible}>
        {filter.values.map(value => <FilterItem key={value.value_name} value={value} />)}
      </StyledValuesUl>
    </StyledFilterBox>
  );
}

export default Filter;

// *** Styles ***
const StyledFilterBox = styled.div`
  width: 80%;
  border-top: 1px solid ${props => props.theme.blackgrey};
  font-size: 1rem;
  padding: 1.25em 0.2em;
`;

const StyledTitleBox = styled.div`
  display: flex;
  justify-content: start;
  align-items: center;
`;

const StyledFilterButton = styled.button`
  border: none;
  box-shadow: none;
  padding: 0;
  background-color: transparent;
  width: 0.5em;
  margin-right: 0.5em;

  h6 {
    font-size: 1em;
  }

  &:focus {
    outline: none;
  }
`;

const StyledValuesUl = styled.ul`
  display: ${props => props.isVisible ? 'block' : 'none'};
  list-style-type: none;
  margin: 0;
  padding: 0.7em 0 0 1em;
`;
