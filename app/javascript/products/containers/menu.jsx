import React from 'react';
//import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux'
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';

import Filter from './filter';

const Menu = (props) => {
  const desktopSideMenu = useMediaQuery({ minWidth: 451});
  const mobileSideMenu = useMediaQuery({ maxWidth: 450});
  const filters = useSelector(state => state.filters);

  return (
    <>
    {desktopSideMenu && <StyledDesktopMenu>
      {filters.map(filter => <Filter key={filter.name} filter={filter} />)}
      <StyledBorder></StyledBorder>
    </StyledDesktopMenu>}
    {mobileSideMenu && <StyledMobileMenu>
      <StyledMobileMenuBox>
        {filters.map(filter => <Filter key={filter.name} filter={filter} />)}
        <StyledBorder></StyledBorder>
      </StyledMobileMenuBox>
      <StyledFilterButton />
    </StyledMobileMenu>}
    </>
  );
}

export default Menu;

// *** Styles ***
const StyledMobileMenu = styled.div`
  position: fixed;
  top: 3rem;
  left: 0;

  width: 250px;
  height: calc(100vh - 3rem);
  padding: 25px 0;

  background-color: rgba(255, 255, 255, .9);
  box-shadow: 1px 0px 4px 0.5px ${props => props.theme.greyBB};
`;

const StyledMobileMenuBox = styled.div`
  /*position: fixed;
  top: 3rem;
  left: 0;

  width: 250px;
  height: calc(100vh - 3rem);
  padding: 25px 0;*/

  width: 100%;
  height: 100%;


  overflow: scroll;
  &::-webkit-scrollbar {
    display: none;
  }

  display: flex;
  flex-direction: column;
  align-items: center;


`;

const StyledDesktopMenu = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  padding-top: 9px;
`;

const StyledFilterButton = styled.div`
  position: fixed;
  z-index: 50;

  top: calc(calc(100vh / 2) - 20px + 1.5rem);
  left: 250px;
  height: 50px;
  width: 50px;

  border-radius: 5px 5px 5px 0;
  background: linear-gradient(90deg, rgba(255,255,255,0.97) 0%, rgba(255,255,255,0.97) 5%, rgba(255,255,255,0.9) 15%, rgba(255,255,255,0.9) 100%);
  box-shadow: 2px 0px 3px 0px ${props => props.theme.greyBB},
              2px -2px 3px 0px ${props => props.theme.greyBB},
              2px 2px 3px 0px ${props => props.theme.greyBB};

  &::before {
    content: "";
    position: absolute;

    background-color: transparent;
    bottom: -10px;
    height: 10px;
    width: 5px;
    border-top-left-radius: 5px;
    box-shadow: 0 -6px 0 0 rgba(255, 255, 255, .9);
  }

  &::after {
    content: "";
    position: absolute;

    background-color: transparent;
    top: -10px;
    height: 10px;
    width: 5px;
    border-bottom-left-radius: 5px;
    box-shadow: 0 6px 0 0 rgba(255, 255, 255, .9);
  }
`;

const StyledBorder = styled.div`
  width: 80%;
  border-top: 1px solid ${props => props.theme.blackgrey};
`;


