import React, { useState } from 'react';
//import { Link } from 'react-router-dom';
//import { useSelector } from 'react-redux'
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';

const FilterItem = (props) => {
  const { value } = props;
  const filterValue = value.filter_num === '-1' ? '' : `[${value.filter_num}]`
  const checkBox =
    value.filter_num === '-1' ? <i className="far fa-times-circle"></i> : <i className="far fa-circle"></i>

  return (
      <StyledFilterItemLi disabled={value.filter_num === '0' ? true : false} filter_num={value.filter_num}>
        <StyledCheckBox>{checkBox}</StyledCheckBox>
        <h6>{`${value.value_name} ${filterValue}`}</h6>
      </StyledFilterItemLi>
  );
}

export default FilterItem;

// *** Styles ***
const StyledFilterItemLi = styled.li`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin: 0.35em 0;

  width: fit-content;

  h6 {
    color: ${props => {
      if (props.disabled) {
        return props.theme.greyBB;
      } else if (props.filter_num === '-1') {
        return props.theme.blackgrey;
      }

      return props.theme.grey77;
    }};
    transition: color 220ms ease;
  }



  &:hover h6 {
      color: ${props => props.disabled ? props.theme.greyBB : props.theme.blackgrey};
      cursor: ${props => props.disabled ? 'auto' : 'pointer'};
    }

`;

const StyledCheckBox = styled.h6`
  margin-right: 0.9em;
`;






// <input
//         type="checkbox"
//         id={value.value_name}
//         name={value.value_name}
//         value={value.value_name}
//         checked={value.filter_num === '-1' ? true : false}
//         disabled={value.filter_num === '0' ? true : false}
//         onChange={(event) => console.log(event)}
//       />
