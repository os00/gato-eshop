import React from 'react';
//import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux'
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';

import ProductItem from './product_item';

const Products = (props) => {
  const products = useSelector(state => state.products);

  return (
    <StyledProductsBox>
      {products.map(product => <ProductItem key={product.id} product={product} />)}
    </StyledProductsBox>
  );
}

export default Products;

// *** Styles ***
const StyledProductsBox = styled.div`
  display: flex;
  justify-content: flex-start;
  align-content: flex-start;
  flex-wrap: wrap;
  width: 100%;
`;


