import React, { useState } from 'react';
//import { Link } from 'react-router-dom';
//import { useSelector } from 'react-redux'
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';

const ProductItemCart = (props) => {
  const { product } = props;
  const [inputInitialValue, setInputInitialValue] = useState(1);
  const [inputValue, setInputValue] = useState(1);


  const inputFocus = (event) => {
    setInputValue(event.currentTarget.value);
  }

  const inputBlur = (event) => {
    let pcs = parseInt(event.currentTarget.value);
    if (isNaN(pcs)) {
      setInputValue(inputInitialValue);
      return;
    }
    if (pcs < 1) { pcs = 1; }
    if (pcs > 99) { pcs = 99; }

    setInputValue(pcs);
    setInputInitialValue(pcs);
  }

  const inputChange = (event) => {
    setInputValue(event.currentTarget.value);
  }

  const plusButtonClick = (event) => {
    event.preventDefault();
    if (inputValue === 99) {
      return;
    }

    setInputValue(inputValue + 1);
    setInputInitialValue(inputValue + 1);
  }

  const minusButtonClick = (event) => {
    event.preventDefault();
    if (inputValue === 1) {
      return;
    }

    setInputValue(inputValue - 1);
    setInputInitialValue(inputValue - 1);
  }

  return (
    <StyledCartBox>
      <h6>Přidat</h6>
      <StyledAddButton onClick={plusButtonClick}>+</StyledAddButton>
      <StyledInput
        type='text'
        value={inputValue}
        onChange={inputChange}
        onBlur={inputBlur}
        onFocus={inputFocus}
        onClick={e => e.preventDefault()}
      />
      <StyledAddButton onClick={minusButtonClick}>-</StyledAddButton>
      <StyledCartButton>
        <span data-content="Do košíku">Do košíku</span>
      </StyledCartButton>
    </StyledCartBox>
  );

}

export default ProductItemCart;

// *** Styles ***
const StyledCartBox = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 10px;
`;

const StyledAddButton = styled.button`
  border: none;
  box-shadow: none;
  padding: 0;
  background-color: transparent;
  width: 0.5em;
  //margin-right: 0.5em;

  h6 {
    font-size: 1em;
  }

  &:focus {
    outline: none;
  }
`;

const StyledInput = styled.input`
  height: 2em;
  width: 2em;
  padding: 0.4em 0.3em;
  font-family: ${props => props.theme.poppinsFont};
  font-size: 0.8125em;
  color: ${props => props.theme.blackgrey};
  text-align: center;
`;

const StyledCartButton = styled.button`
    position: relative;
    border: none;
    padding: 0;
    transition: clip-path 275ms ease;

    &:hover span::before, &:focus span::before {
      clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
      outline: none;
    }

    span {
      position: relative;
      display: inline-block;
      color: ${props => props.theme.blackgrey};
      font-family: ${props => props.theme.poppinsFont};
      font-weight: 600;
      font-size: 0.8125em;
      padding: 0.3em 1.5em;
      background-color: ${props => props.theme.yellow};
      border-radius: 3px;

      &::before {
        position: absolute;
        content: attr(data-content);
        color: ${props => props.theme.blackgrey};
        text-decoration: underline;
        text-decoration-color: ${props => props.theme.blackgrey};
        clip-path: polygon(0 0, 0 0, 0% 100%, 0 100%);
        transition: clip-path 275ms ease;
      }
    }
  `;
