import React, { useState } from 'react';
//import { Link } from 'react-router-dom';
//import { useSelector } from 'react-redux'
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';

import ProductItemCart from './product_item_cart';

const ProductItem = (props) => {
  const { product } = props;

  return (
    <StyledProductBox>
      <a href={`/products/${product.id}`}>
        <StyledImageBox>
          <img
            src={product.photos[0].path}
            alt={`${product.brief_description} - overall photo`}
            width="140"
            height="140"
          />
          <StyledSideImages>
            <img
              src={product.photos[1].path}
              alt={`${product.brief_description} - side profile detail`}
              width="66"
              height="66"
            />
            <img
              src={product.photos[2].path}
              alt={`${product.brief_description} - color detail`}
              width="66"
              height="66"
            />
          </StyledSideImages>
        </StyledImageBox>
        <StyledTitleBox>
          <h2>{product.name}</h2>
          <h3>{`${Math.round(product.price)} Kč/ks`}</h3>
        </StyledTitleBox>
        <StyledDescBox>
          <p>{product.brief_description}</p>
          <p>{`${product.length} m/ks`}</p>
        </StyledDescBox>
        <ProductItemCart />
      </a>
    </StyledProductBox>
  );
}

export default ProductItem;

// *** Styles ***
const StyledProductBox = styled.div`
  margin: 0 14px 30px 14px;
  width: 232px;
  padding: 9px;
  border-radius: 2px;
  border: 1px solid transparent;
  transition-property: box-shadow, border;
  transition-duration: 400ms;
  transition-timing-function: ease;

  &:hover {
    box-shadow: 0 0 4px 1px ${props => props.theme.greyE5};
    border: 1px solid ${props => props.theme.greyE5};
  }
`;

const StyledImageBox = styled.div`
  display: flex;
  justify-content: start;
`;

const StyledSideImages = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin-left: 6px;
`;

const StyledTitleBox = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;

  margin: 8px 0;
`;

const StyledDescBox = styled.div`

`;

const StyledCartBox = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const StyledAddButton = styled.button`
  border: none;
  box-shadow: none;
  padding: 0;
  background-color: transparent;
  width: 0.5em;
  margin-right: 0.5em;

  h6 {
    font-size: 1em;
  }

  &:focus {
    outline: none;
  }
`;


