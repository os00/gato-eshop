import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { createHistory as history } from 'history';
import { reducer as formReducer } from 'redux-form';

import App from './components/app';
import productsReducer from './reducers/products_reducer';
import filtersReducer from './reducers/filters_reducer';

const productsContainer = document.getElementById('products_app');

const reducers = combineReducers({
  products: productsReducer,
  filters: filtersReducer,
});

const initialState = {
  products: [
    {"id":19,"name":"HK","brief_description":"Horní kolej Fe zlatá","description":"Horní kolej z oceli zlaté\n                  barvy. V koleji pojíždějí dveře vestavěné skříně.","photos":[{"id":55,"path":"assets/products/18/P01-01.jpg","description":"","order":"0","product_id":19,"created_at":"2020-07-02T17:03:23.421Z","updated_at":"2020-07-02T17:03:23.421Z"},{"id":56,"path":"assets/products/18/P01-02.jpg","description":"","order":"1","product_id":19,"created_at":"2020-07-02T17:03:23.423Z","updated_at":"2020-07-02T17:03:23.423Z"},{"id":57,"path":"assets/products/18/P01-03.jpg","description":"","order":"2","product_id":19,"created_at":"2020-07-02T17:03:23.424Z","updated_at":"2020-07-02T17:03:23.424Z"}],"price":"332.0","length":"5"},
    {"id":1,"name":"HK","brief_description":"Horní kolej Al stříbrná","description":"Horní kolej z extrudovaného hliníku stříbrné\n                  barvy. V koleji pojíždějí dveře vestavěné skříně.","photos":[{"id":1,"path":"assets/products/0/P01-01.jpg","description":"","order":"0","product_id":1,"created_at":"2020-07-02T17:03:23.333Z","updated_at":"2020-07-02T17:03:23.333Z"},{"id":2,"path":"assets/products/0/P01-02.jpg","description":"","order":"1","product_id":1,"created_at":"2020-07-02T17:03:23.335Z","updated_at":"2020-07-02T17:03:23.335Z"},{"id":3,"path":"assets/products/0/P01-03.jpg","description":"","order":"2","product_id":1,"created_at":"2020-07-02T17:03:23.337Z","updated_at":"2020-07-02T17:03:23.337Z"}],"price":"399.0","length":"5"},
    {"id":8,"name":"HK","brief_description":"Horní kolej Al zlatá","description":"Horní kolej z extrudovaného hliníku zlaté\n                  barvy. V koleji pojíždějí dveře vestavěné skříně.","photos":[{"id":22,"path":"assets/products/7/P02-01.jpg","description":"","order":"0","product_id":8,"created_at":"2020-07-02T17:03:23.368Z","updated_at":"2020-07-02T17:03:23.368Z"},{"id":23,"path":"assets/products/7/P02-02.jpg","description":"","order":"1","product_id":8,"created_at":"2020-07-02T17:03:23.370Z","updated_at":"2020-07-02T17:03:23.370Z"},{"id":24,"path":"assets/products/7/P02-03.jpg","description":"","order":"2","product_id":8,"created_at":"2020-07-02T17:03:23.372Z","updated_at":"2020-07-02T17:03:23.372Z"}],"price":"499.0","length":"5"},
    {"id":37,"name":"HK","brief_description":"Horní kolej Fe javor","description":"Horní kolej z válcované oceli s javorovým dřevodekorem.\n                  V koleji pojíždějí dveře vestavěné skříně.","photos":[{"id":109,"path":"assets/products/36/P01-01.jpg","description":"","order":"0","product_id":37,"created_at":"2020-07-02T17:03:23.499Z","updated_at":"2020-07-02T17:03:23.499Z"},{"id":110,"path":"assets/products/36/P01-02.jpg","description":"","order":"1","product_id":37,"created_at":"2020-07-02T17:03:23.500Z","updated_at":"2020-07-02T17:03:23.500Z"},{"id":111,"path":"assets/products/36/P01-03.jpg","description":"","order":"2","product_id":37,"created_at":"2020-07-02T17:03:23.502Z","updated_at":"2020-07-02T17:03:23.502Z"}],"price":"448.0","length":"5"},
    {"id":28,"name":"HK","brief_description":"Horní kolej Fe bílá","description":"Horní kolej z oceli bílé\n                  barvy. V koleji pojíždějí dveře vestavěné skříně.","photos":[{"id":82,"path":"assets/products/27/P01-01.jpg","description":"","order":"0","product_id":28,"created_at":"2020-07-02T17:03:23.460Z","updated_at":"2020-07-02T17:03:23.460Z"},{"id":83,"path":"assets/products/27/P01-02.jpg","description":"","order":"1","product_id":28,"created_at":"2020-07-02T17:03:23.461Z","updated_at":"2020-07-02T17:03:23.461Z"},{"id":84,"path":"assets/products/27/P01-03.jpg","description":"","order":"2","product_id":28,"created_at":"2020-07-02T17:03:23.463Z","updated_at":"2020-07-02T17:03:23.463Z"}],"price":"332.0","length":"5"}
  ],
  "filters": [
    {
      "name": "Typ profilu", "values": [
                               { "value_name": "DR", "filter_num": "+ 6" },
                               { "value_name": "HK", "filter_num": "+ 5" },
                               { "value_name": "HR", "filter_num": "-1" },
                               { "value_name": "SK", "filter_num": "+ 5" },
                               { "value_name": "VR", "filter_num": "+ 17" }
                              ]
    },
    {
      "name": "Drážka", "values": [
                          { "value_name": "10", "filter_num": "32" },
                          { "value_name": "16", "filter_num": "0" },
                          { "value_name": "4", "filter_num": "15" }
                        ]
    },
    {
      "name": "Materiál", "values": [
                            { "value_name": "Hliník", "filter_num": "18" },
                            { "value_name": "Ocel", "filter_num": "27" }
                          ]
    },
    {
      "name": "Typ barvy", "values": [
                             { "value_name": "Dřevodekor", "filter_num": "9" },
                             { "value_name": "Základní barva", "filter_num": "36" }
                           ]
    },
    {
      "name": "Barva", "values": [
                         { "value_name": "Bílá", "filter_num": "0" },
                         { "value_name": "Javor", "filter_num": "9" },
                         { "value_name": "Stříbrná", "filter_num": "11" },
                         { "value_name": "Zlatá", "filter_num": "16" }
                        ]
    }
  ]
}

const middlewares = applyMiddleware(thunk, logger);

ReactDOM.render(
  <Provider store={createStore(reducers, initialState, middlewares)}>
    <Router history={history}>
     <Switch>
       <Route path="/products" component={App} />
     </Switch>
   </Router>
  </Provider>,
  productsContainer
);
