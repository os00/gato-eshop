class ApplicationMailer < ActionMailer::Base
  default from: 'web@gato.cz'
  layout 'mailer'
end
