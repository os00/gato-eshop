class ContactFormMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact_form_mailer.new_contact_message.subject
  #
  def new_contact_message(message)
    @message = message
    @contact = message.contact

    # @message_web = @message.message.gsub("\r", '')
    @subject_underline = @message.subject.gsub(/./, '_')

    mail to: 'ondrej.svejkovsky@gmail.com',
         subject: "Nová zpráva z kontaktního formuláře: #{@message.subject}"
  end
end
