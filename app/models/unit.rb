# frozen_string_literal: true

# Units used by products and parameters
class Unit < ApplicationRecord
  has_many :products
  has_many :parameters

  validates :name, presence: true
  validates :name, uniqueness: true
end
