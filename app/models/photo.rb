class Photo < ApplicationRecord
  belongs_to :product

  validates :path, :order, presence: true
end
