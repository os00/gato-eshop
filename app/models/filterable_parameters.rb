# frozen_string_literal: true

# View in DB which is used to obtain all ordered filterable parameters
class FilterableParameters < ApplicationRecord
end
