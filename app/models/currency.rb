# frozen_string_literal: true

# Currency represented by a 3-letter currency code
class Currency < ApplicationRecord
  has_many :price_lists
  has_many :orders

  validates :currency, presence: true
  validates :currency, uniqueness: true
  validates :currency,
            format: {
              with: /\A[A-Z]{3}\z/,
              message: 'Currency code consists of only three capital letters'
            }
end
