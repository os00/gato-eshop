# frozen_string_literal: true

# Single product of the eshop
class Product < ApplicationRecord
  has_many :product_categories, dependent: :destroy
  has_many :categories, through: :product_categories
  has_many :product_tags, dependent: :destroy
  has_many :tags, through: :product_tags
  has_many :product_parameters, dependent: :destroy
  has_many :parameters_direct, through: :product_parameters,
                               source: :parameter
  has_many :prices
  has_many :photos
  belongs_to :unit

  validates :unit_id, :name, presence: true

  def parameters
    parameters = []
    product_parameters.each do |param|
      result = {}
      result[:name] = param.parameter.name
      result[:value] = to_number(param.value)
      result[:unit] = param.parameter.unit.nil? ? '' : param.parameter.unit.name
      result[:value_printable] =
        result[:unit] == '' ? param.value : "#{param.value} #{result[:unit]}"
      parameters << result
    end

    return parameters
  end

  private

  def to_number(input_string)
    number = BigDecimal(input_string)
  rescue ArgumentError
    return input_string
  else
    return number.frac.zero? ? number.to_i : number
  end
end
