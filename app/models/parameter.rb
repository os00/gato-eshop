# frozen_string_literal: true

# Parameter of an product characterized by name and unit
class Parameter < ApplicationRecord
  belongs_to :unit, optional: true
  has_many :product_parameters, dependent: :destroy
  has_many :products, through: :product_parameters

  validates :name, :filter_order, presence: true
  validates :name, uniqueness: true
  validates :is_filterable, inclusion: { in: [true, false] }
end
