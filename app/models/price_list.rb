# frozen_string_literal: true

# Price list in a currency - either general or belonging to a specific user
class PriceList < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :currency
  has_many :prices

  validates :currency_id, :name, presence: true
  validates :name, uniqueness: true
  validates :is_primary, inclusion: { in: [true, false] }
end
