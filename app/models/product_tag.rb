# frozen_string_literal: true

# Creates many to many relationship between products and tags
class ProductTag < ApplicationRecord
  belongs_to :product
  belongs_to :tag

  validates :tag_id, :product_id, presence: true
end
