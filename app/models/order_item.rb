# frozen_string_literal: true

# Specific item of an order
class OrderItem < ApplicationRecord
  belongs_to :product
  belongs_to :order

  validates :product_id, :order_id, :quantity, :price, presence: true
  validates :quantity, :price, numericality: { greater_than_or_equal_to: 0 }
end
