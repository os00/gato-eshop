# frozen_string_literal: true

# Creates many to many relationship between coupons and parameters
class CouponParameter < ApplicationRecord
  belongs_to :coupon
  belongs_to :parameter

  validates :coupon_id, :parameter_id, :value, presence: true
end
