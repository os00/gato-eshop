# frozen_string_literal: true

# User info (for registered or unregistered users)
class User < ApplicationRecord
  has_one :contact
  has_many :user_discounts, dependent: :destroy
  has_many :discounts, through: :user_discounts
  has_one :price_list
  #has_many :orders, through: :contact

  enum role: { undefined: 1 }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable, :confirmable

  validates :email, :role, presence: true
  validates :email, uniqueness: true
  validates :email, format: {
    with: /([a-z0-9_-]+(\.[a-z0-9_-]+)*)@[a-z0-9_-]+[.]{1}([a-z0-9_-]+(\.[a-z0-9_-]+)*)/,
    message: 'incorrect mail format'
  }
  validates :is_active, inclusion: { in: [true, false] }

  def destroy
    update_attributes(is_active: false) if is_active
  end
end
