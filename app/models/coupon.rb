# frozen_string_literal: true

# Coupons used for one time discounts
class Coupon < ApplicationRecord
  has_many :coupon_parameters, dependent: :destroy
  has_many :parameters_direct, through: :coupon_parameters,
                               source: :parameter

  validates :code, :absolute_value, :percent_value,
            presence: true
  validates :code, uniqueness: true
  validates :is_active, inclusion: { in: [true, false] }
  validates :absolute_value, numericality: { greater_than_or_equal_to: 0 }
  validates :percent_value,
            numericality: {
              greater_than_or_equal_to: 0,
              less_than_or_equal_to: 100
            }

  def parameters
    parameters = []
    coupon_parameters.each do |param|
      result = {}
      result[:name] = param.parameter.name
      result[:value] = to_number(param.value)
      result[:unit] = param.parameter.unit.nil? ? '' : param.parameter.unit.name
      result[:value_printable] =
        result[:unit] == '' ? param.value : "#{param.value} #{result[:unit]}"
      parameters << result
    end

    return parameters
  end

  private

  def to_number(input_string)
    number = BigDecimal(input_string)
  rescue ArgumentError
    return input_string
  else
    return number.frac.zero? ? number.to_i : number
  end
end
