# frozen_string_literal: true

# Order issued by a user
class Order < ApplicationRecord
  belongs_to :contact
  belongs_to :currency
  has_one :invoice
  has_one :shipping
  has_one :payment
  has_many :order_items

  enum status: { received: 10, processing: 20, shipped: 30, closed: 40 }

  delegate :user, to: :contact, allow_nil: true

  validates :number, :date, :total, :status,
            :contact_id, :currency_id, presence: true

  validates :number, uniqueness: true
  # validates :date, not_in_past: true
  validates :total, numericality: { greater_than_or_equal_to: 0 }

  def total
    payment_price = payment.nil? ? 0 : payment.price
    shipping_price = shipping.nil? ? 0 : shipping.price
    result = order_items.sum(:price) + payment_price + shipping_price
    write_attribute(:total, result)

    return result
  end
end
