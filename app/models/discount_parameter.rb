# frozen_string_literal: true

# Creates many to many relationship between discounts and parameters
class DiscountParameter < ApplicationRecord
  belongs_to :parameter
  belongs_to :discount

  validates :parameter_id, :discount_id, :value, presence: true
end
