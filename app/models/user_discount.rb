# frozen_string_literal: true

# Creates many to many relationship between users and discounts
class UserDiscount < ApplicationRecord
  belongs_to :discount
  belongs_to :user

  validates :discount_id, :user_id, presence: true
end
