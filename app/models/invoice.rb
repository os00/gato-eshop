# frozen_string_literal: true

# Invoice related data, invoice is part of an order
class Invoice < ApplicationRecord
  belongs_to :order

  validates :date, :due_date, :number, presence: true
  validates :number, uniqueness: true
  # validates :date, not_in_past: true
end
