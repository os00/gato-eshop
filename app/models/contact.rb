# frozen_string_literal: true

# Contact data (name) of user
class Contact < ApplicationRecord
  belongs_to :user, optional: true
  has_many :addresses
  has_many :orders
  has_one :company
  has_many :messages, inverse_of: :contact
  accepts_nested_attributes_for :messages

  validates :first_name, :last_name, :email, presence: { message: 'Povinné pole' }
  validates :user_id, uniqueness: true, allow_nil: true
  validates :email, format: {
    with: /([a-z0-9_-]+(\.[a-z0-9_-]+)*)@[a-z0-9_-]+[.]{1}([a-z0-9_-]+(\.[a-z0-9_-]+)*)/,
    message: 'Nesprávý formát emailu'
  }, uniqueness: true
end
