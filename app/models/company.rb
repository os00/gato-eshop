# frozen_string_literal: true

# Basic company data (belonging to user)
class Company < ApplicationRecord
  belongs_to :contact
  has_one :address

  validates :contact_id, :name, :id_number, :vat_id_number, presence: true
  validates :contact_id, uniqueness: true
end
