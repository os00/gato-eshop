# frozen_string_literal: true

# Payment processing info belonging to an order
class Payment < ApplicationRecord
  belongs_to :order

  enum status: { not_paid: 10, processing: 20, paid: 30 }
  enum method: { cc: 10, cash: 20, cod: 30, transfer: 40 }

  validates :order_id, :price, :number, :status, :method, presence: true
  validates :number, uniqueness: true
  validates :price, numericality: { greater_than_or_equal_to: 0 }
end
