# frozen_string_literal: true

# category of products (with possible tree structure)
class Category < ApplicationRecord
  has_many :discount_categories, dependent: :destroy
  has_many :categories, through: :discount_categories
  has_many :product_categories, dependent: :destroy
  has_many :products, through: :product_categories

  validates :name, :parent_category, presence: true
  validates :name, uniqueness: true
  validates :parent_category, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0
  }
end
