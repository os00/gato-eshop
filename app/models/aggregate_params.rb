# frozen_string_literal: true

# View in DB which is used to obtain products filtered according to
# parameter values
#
# SQL of the view:
# CREATE VIEW aggregate_params AS (
#         SELECT products.*, ARRAY_AGG(parameters.name || '=' || product_parameters.value) AS ag_params
#         FROM products
#         INNER JOIN product_parameters ON product_parameters.product_id = products.id
#         INNER JOIN parameters ON product_parameters.parameter_id = parameters.id
#         GROUP BY products.id
class AggregateParams < ApplicationRecord
  self.primary_key = :id

  def as_json(options = {})
    aggregate_params_as_json = super.as_json
    aggregate_params_as_json[:photos] = Photo.where(product_id: id).limit(3)
    aggregate_params_as_json[:price] = get_price(options[:current_user], id)

    aggregate_params_as_json[:length] =
      ProductParameter.joins(:parameter).joins(:product)
                      .where('parameters.name = ? AND products.id = ?', 'Délka', id)
                      .first.value

    aggregate_params_as_json
  end

  private

  def get_price(current_user, product_id)
    current_user_price = []
    unless current_user.price_list.nil?
      current_user_price =
        Price.joins(:price_list).joins(:product)
             .where('price_lists.user_id = ? AND products.id = ?', current_user.id, product_id)
    end

    return current_user_price.first.price unless current_user_price.count.zero?

    Price.joins(:price_list).joins(:product)
         .where('price_lists.is_primary = ? AND products.id = ?', true, product_id)
         .first.price
  end
end
