# frozen_string_literal: true

# Address for shipping or as an official company address
class Address < ApplicationRecord
  belongs_to :contact, optional: true
  belongs_to :company, optional: true
  has_many :shippings

  validates :street, :number, :city,
            :zip, :country, presence: true
  validate :contact_or_company_presence

  private

  def contact_or_company_presence
    if contact_id.blank? && company_id.blank?
      errors.add(:base, 'Adress has to be linked to contact or company or both')
    end
  end
end
