# frozen_string_literal: true

# Shipping processing info (to specific address) belonging to an order
class Shipping < ApplicationRecord
  belongs_to :address
  belongs_to :order

  enum status: { processing: 10, ready: 20, handed_over: 30, delivered: 40 }
  enum method: { pickup: 10, toptrans: 20, post: 30 }

  validates :address_id, :order_id, :price, :number, :status, :method,
            presence: true
  validates :number, uniqueness: true
  validates :price, numericality: { greater_than_or_equal_to: 0 }
end
