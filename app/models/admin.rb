# frozen_string_literal: true

# admin role for administration of the e-shop
class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable

  validates :email, :password, presence: true
  validates :email, uniqueness: true
  validates :email, format: {
    with: /([a-z0-9_-]+(\.[a-z0-9_-]+)*)@[a-z0-9_-]+[.]{1}([a-z0-9_-]+(\.[a-z0-9_-]+)*)/,
    message: 'incorrect mail format'
  }
  validates :is_active, inclusion: { in: [true, false] }

  def destroy
    update_attributes(is_active: false) if is_active
  end

  def active_for_authentication?
    super && is_active
  end
end
