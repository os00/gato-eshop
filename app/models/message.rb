# frozen_string_literal: true

# Messages from visitors of the web
class Message < ApplicationRecord
  belongs_to :contact, inverse_of: :messages

  validates :contact, :subject, :message, presence: { message: 'Povinné pole' }
end
