# frozen_string_literal: true

# Discounts linked to specific categories or users
class Discount < ApplicationRecord
  has_many :discount_parameters, dependent: :destroy
  has_many :parameters_direct, through: :discount_parameters,
                               source: :parameter
  has_many :user_discounts, dependent: :destroy
  has_many :users, through: :user_discounts

  validates :percent_value, presence: true
  validates :is_active, inclusion: { in: [true, false] }
  validates :percent_value,
            numericality: {
              greater_than_or_equal_to: 0,
              less_than_or_equal_to: 100
            }
  validates :turnover_value, numericality: { greater_than_or_equal_to: 0 },
                             allow_blank: true
  validates :turnover_period,
            numericality: { only_integer: true, greater_than_or_equal_to: 0 },
            allow_blank: true

  def parameters
    parameters = []
    discount_parameters.each do |param|
      result = {}
      result[:name] = param.parameter.name
      result[:value] = to_number(param.value)
      result[:unit] = param.parameter.unit.nil? ? '' : param.parameter.unit.name
      result[:value_printable] =
        result[:unit] == '' ? param.value : "#{param.value} #{result[:unit]}"
      parameters << result
    end

    return parameters
  end

  private

  def to_number(input_string)
    number = BigDecimal(input_string)
  rescue ArgumentError
    return input_string
  else
    return number.frac.zero? ? number.to_i : number
  end
end
