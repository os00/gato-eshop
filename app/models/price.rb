# frozen_string_literal: true

# Single price in a price list
class Price < ApplicationRecord
  belongs_to :product
  belongs_to :price_list

  validates :product_id, :price_list_id, :price, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 0 }
  validates :price_list, uniqueness: { scope: :product }
end
