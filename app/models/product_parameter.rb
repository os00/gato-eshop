# frozen_string_literal: true

# Creates many to many relationship between products and parameters
class ProductParameter < ApplicationRecord
  belongs_to :product
  belongs_to :parameter

  validates :parameter_id, :product_id, presence: true
end
