# frozen_string_literal: true

# Single tag of an product
class Tag < ApplicationRecord
  has_many :product_tags, dependent: :destroy
  has_many :products, through: :product_tags

  validates :name, presence: true
  validates :name, uniqueness: true
end
