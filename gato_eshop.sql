CREATE TABLE `admin` (
  `id` int UNIQUE PRIMARY KEY,
  `email` text NOT NULL DEFAULT "",
  `encrypted_password` text NOT NULL DEFAULT "",
  `is_active` boolean NOT NULL DEFAULT true,
  `reset_password_token` text,
  `reset_password_sent_at` datetime,
  `remember_created_at` datetime,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sign_in_count` integer NOT NULL DEFAULT 0,
  `current_sign_in_at` datetime,
  `last_sign_in_at` datetime,
  `current_sign_in_ip` text,
  `last_sign_in_ip` text
);

CREATE TABLE `users` (
  `id` int UNIQUE PRIMARY KEY,
  `email` text NOT NULL DEFAULT "",
  `encrypted_password` text NOT NULL DEFAULT "",
  `role` text,
  `is_active` boolean NOT NULL DEFAULT true,
  `reset_password_token` text,
  `reset_password_sent_at` datetime,
  `remember_created_at` datetime,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sign_in_count` integer NOT NULL DEFAULT 0,
  `current_sign_in_at` datetime,
  `last_sign_in_at` datetime,
  `current_sign_in_ip` text,
  `last_sign_in_ip` text
);

CREATE TABLE `contacts` (
  `id` int UNIQUE PRIMARY KEY,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `user_id` int UNIQUE NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE `adresses` (
  `id` int UNIQUE PRIMARY KEY,
  `street` text NOT NULL,
  `number` text NOT NULL,
  `city` text NOT NULL,
  `zip` text NOT NULL,
  `country` text,
  `is_primary` boolean NOT NULL DEFAULT false,
  `user_id` int NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE `companies` (
  `id` int UNIQUE PRIMARY KEY,
  `name` text NOT NULL,
  `ic` text NOT NULL,
  `dic` text,
  `user_id` int UNIQUE NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE `products` (
  `id` int UNIQUE PRIMARY KEY,
  `name` text NOT NULL,
  `brief_description` text,
  `description` text
);

CREATE TABLE `categories` (
  `id` int UNIQUE PRIMARY KEY,
  `name` text NOT NULL,
  `description` text,
  `parent_category` int NOT NULL DEFAULT 0
);

CREATE TABLE `product_categories` (
  `id` int UNIQUE PRIMARY KEY,
  `category_id` int NOT NULL,
  `product_id` int NOT NULL,
  FOREIGN KEY (category_id) REFERENCES categories(id),
  FOREIGN KEY (product_id) REFERENCES products(id)
);

CREATE TABLE `parameters` (
  `id` int UNIQUE PRIMARY KEY,
  `name` text NOT NULL,
  `unit` text
);

CREATE TABLE `product_parameters` (
  `id` int UNIQUE PRIMARY KEY,
  `value` text,
  `product_id` int NOT NULL,
  `parameter_id` int NOT NULL,
  FOREIGN KEY (product_id) REFERENCES products(id),
  FOREIGN KEY (parameter_id) REFERENCES parameters(id)
);

CREATE TABLE `tags` (
  `id` int UNIQUE PRIMARY KEY,
  `name` text NOT NULL
);

CREATE TABLE `product_tags` (
  `id` int UNIQUE PRIMARY KEY,
  `product_id` int NOT NULL,
  `tag_id` int NOT NULL,
  FOREIGN KEY (product_id) REFERENCES products(id),
  FOREIGN KEY (tag_id) REFERENCES tags(id)
);

CREATE TABLE `currencies` (
  `id` int UNIQUE PRIMARY KEY,
  `currency` text NOT NULL
);

CREATE TABLE `price_lists` (
  `id` int UNIQUE PRIMARY KEY,
  `name` text NOT NULL,
  `user_id` int UNIQUE NOT NULL,
  `currency_id` int UNIQUE NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (currency_id) REFERENCES currencies(id)
);

CREATE TABLE `prices` (
  `id` int UNIQUE PRIMARY KEY,
  `price` decimal NOT NULL,
  `product_id` int NOT NULL,
  `price_list_id` int NOT NULL,
  FOREIGN KEY (product_id) REFERENCES products(id),
  FOREIGN KEY (price_list_id) REFERENCES price_lists(id)
);

CREATE TABLE `discounts` (
  `id` int UNIQUE PRIMARY KEY,
  `start_date` datetime,
  `end_date` datetime,
  `turnover_value` decimal,
  `turnover_period` int,
  `absolute_value` decimal NOT NULL DEFAULT 0,
  `percent_value` decimal NOT NULL DEFAULT 0
);

CREATE TABLE `discount_categories` (
  `id` int UNIQUE PRIMARY KEY,
  `category_id` int NOT NULL,
  `discount_id` int NOT NULL,
  FOREIGN KEY (category_id) REFERENCES categories(id),
  FOREIGN KEY (discount_id) REFERENCES discounts(id)
);

CREATE TABLE `user_discounts` (
  `id` int UNIQUE PRIMARY KEY,
  `discount_id` int NOT NULL,
  `user_id` int NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (discount_id) REFERENCES discounts(id)
);

CREATE TABLE `coupons` (
  `id` int UNIQUE PRIMARY KEY,
  `code` text,
  `start_date` datetime,
  `end_date` datetime,
  `absolute_value` decimal NOT NULL DEFAULT 0,
  `percent_value` decimal NOT NULL DEFAULT 0
);

CREATE TABLE `coupon_categories` (
  `id` int UNIQUE PRIMARY KEY,
  `coupon_id` int NOT NULL,
  `category_id` int NOT NULL,
  FOREIGN KEY (coupon_id) REFERENCES coupons(id),
  FOREIGN KEY (category_id) REFERENCES categories(id)
);

CREATE TABLE `orders` (
  `id` int UNIQUE PRIMARY KEY,
  `number` int NOT NULL,
  `date` date NOT NULL,
  `total` decimal NOT NULL DEFAULT 0,
  `status` int NOT NULL,
  `user_id` int NOT NULL,
  `currency_id` int UNIQUE NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (currency_id) REFERENCES currencies(id)
);

CREATE TABLE `order_items` (
  `id` int UNIQUE PRIMARY KEY,
  `quantity` decimal NOT NULL DEFAULT 0,
  `price` decimal NOT NULL DEFAULT 0,
  `product_id` int NOT NULL,
  `order_id` int NOT NULL,
  FOREIGN KEY (product_id) REFERENCES products(id),
  FOREIGN KEY (order_id) REFERENCES orders(id)
);

CREATE TABLE `shippings` (
  `id` int UNIQUE PRIMARY KEY,
  `price` decimal NOT NULL DEFAULT 0,
  `number` text,
  `status` int NOT NULL,
  `method` int NOT NULL,
  `address_id` int NOT NULL,
  `order_id` int NOT NULL,
  FOREIGN KEY (address_id) REFERENCES addresses(id),
  FOREIGN KEY (order_id) REFERENCES orders(id)
);

CREATE TABLE `payment` (
  `id` int UNIQUE PRIMARY KEY,
  `price` decimal NOT NULL DEFAULT 0,
  `number` text,
  `status` int NOT NULL,
  `method` int NOT NULL,
  `order_id` int NOT NULL,
  FOREIGN KEY (order_id) REFERENCES orders(id)
);

CREATE TABLE `invoices` (
  `id` int UNIQUE PRIMARY KEY,
  `date` date NOT NULL,
  `due_date` date NOT NULL,
  `number` int NOT NULL,
  `order_id` int NOT NULL,
  FOREIGN KEY (order_id) REFERENCES orders(id)
);
