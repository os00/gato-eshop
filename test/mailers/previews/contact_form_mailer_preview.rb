# Preview all emails at http://localhost:3000/rails/mailers/contact_form_mailer
class ContactFormMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/contact_form_mailer/new_contact_message
  def new_contact_message
    ContactFormMailer.new_contact_message(Message.last)
  end

end
