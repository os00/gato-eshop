require 'test_helper'

class ContactFormMailerTest < ActionMailer::TestCase
  test "new_contact_message" do
    mail = ContactFormMailer.new_contact_message
    assert_equal "New contact message", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
