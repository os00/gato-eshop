Rails.application.routes.draw do

  root to: 'home#index'

  post 'home', to: 'home#create'
  get  'home', to: 'home#index'

  get  '/products', to: 'products#index'

  # API
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :products, only: [:index]
      resources :filters, only: [:index]
    end
  end

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :admins
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
