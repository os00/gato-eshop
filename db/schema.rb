# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_06_120745) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "street"
    t.string "number"
    t.string "city"
    t.string "zip"
    t.string "country"
    t.bigint "contact_id"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_addresses_on_company_id"
    t.index ["contact_id"], name: "index_addresses_on_contact_id"
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.integer "parent_category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "id_number"
    t.string "vat_id_number"
    t.bigint "contact_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_companies_on_contact_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone"
    t.string "email"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "coupon_parameters", force: :cascade do |t|
    t.string "value"
    t.bigint "coupon_id"
    t.bigint "parameter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["coupon_id"], name: "index_coupon_parameters_on_coupon_id"
    t.index ["parameter_id"], name: "index_coupon_parameters_on_parameter_id"
  end

  create_table "coupons", force: :cascade do |t|
    t.string "code"
    t.string "description"
    t.boolean "is_active"
    t.datetime "start_date"
    t.datetime "end_date"
    t.decimal "absolute_value"
    t.decimal "percent_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "currencies", force: :cascade do |t|
    t.string "currency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "discount_parameters", force: :cascade do |t|
    t.string "value"
    t.bigint "parameter_id"
    t.bigint "discount_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discount_id"], name: "index_discount_parameters_on_discount_id"
    t.index ["parameter_id"], name: "index_discount_parameters_on_parameter_id"
  end

  create_table "discounts", force: :cascade do |t|
    t.boolean "is_active"
    t.string "description"
    t.datetime "start_date"
    t.datetime "end_date"
    t.decimal "turnover_value"
    t.integer "turnover_period"
    t.decimal "percent_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoices", force: :cascade do |t|
    t.date "date"
    t.date "due_date"
    t.string "number"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_invoices_on_order_id"
  end

  create_table "messages", force: :cascade do |t|
    t.string "subject"
    t.text "message"
    t.bigint "contact_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_messages_on_contact_id"
  end

  create_table "order_items", force: :cascade do |t|
    t.decimal "quantity"
    t.decimal "price"
    t.bigint "product_id"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["product_id"], name: "index_order_items_on_product_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "number"
    t.text "note"
    t.date "date"
    t.decimal "total"
    t.integer "status"
    t.bigint "contact_id"
    t.bigint "currency_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_orders_on_contact_id"
    t.index ["currency_id"], name: "index_orders_on_currency_id"
  end

  create_table "parameters", force: :cascade do |t|
    t.string "name"
    t.bigint "unit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_filterable"
    t.string "filter_order"
    t.index ["unit_id"], name: "index_parameters_on_unit_id"
  end

  create_table "payments", force: :cascade do |t|
    t.decimal "price"
    t.string "number"
    t.integer "status"
    t.integer "method"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_payments_on_order_id"
  end

  create_table "photos", force: :cascade do |t|
    t.string "path"
    t.string "description"
    t.string "order"
    t.bigint "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_photos_on_product_id"
  end

  create_table "price_lists", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.boolean "is_primary"
    t.bigint "user_id"
    t.bigint "currency_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["currency_id"], name: "index_price_lists_on_currency_id"
    t.index ["user_id"], name: "index_price_lists_on_user_id"
  end

  create_table "prices", force: :cascade do |t|
    t.decimal "price"
    t.bigint "product_id"
    t.bigint "price_list_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["price_list_id", "product_id"], name: "index_prices_on_price_list_id_and_product_id", unique: true
    t.index ["price_list_id"], name: "index_prices_on_price_list_id"
    t.index ["product_id"], name: "index_prices_on_product_id"
  end

  create_table "product_categories", force: :cascade do |t|
    t.bigint "category_id"
    t.bigint "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_product_categories_on_category_id"
    t.index ["product_id"], name: "index_product_categories_on_product_id"
  end

  create_table "product_parameters", force: :cascade do |t|
    t.string "value"
    t.bigint "product_id"
    t.bigint "parameter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["parameter_id"], name: "index_product_parameters_on_parameter_id"
    t.index ["product_id"], name: "index_product_parameters_on_product_id"
  end

  create_table "product_tags", force: :cascade do |t|
    t.bigint "product_id"
    t.bigint "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_tags_on_product_id"
    t.index ["tag_id"], name: "index_product_tags_on_tag_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "brief_description"
    t.text "description"
    t.bigint "unit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["unit_id"], name: "index_products_on_unit_id"
  end

  create_table "shippings", force: :cascade do |t|
    t.decimal "price"
    t.string "number"
    t.integer "status"
    t.integer "method"
    t.bigint "address_id"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["address_id"], name: "index_shippings_on_address_id"
    t.index ["order_id"], name: "index_shippings_on_order_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "units", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_discounts", force: :cascade do |t|
    t.bigint "discount_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discount_id"], name: "index_user_discounts_on_discount_id"
    t.index ["user_id"], name: "index_user_discounts_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "role", default: 0, null: false
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "addresses", "companies"
  add_foreign_key "addresses", "contacts"
  add_foreign_key "companies", "contacts"
  add_foreign_key "contacts", "users"
  add_foreign_key "coupon_parameters", "coupons"
  add_foreign_key "coupon_parameters", "parameters"
  add_foreign_key "discount_parameters", "discounts"
  add_foreign_key "discount_parameters", "parameters"
  add_foreign_key "invoices", "orders"
  add_foreign_key "messages", "contacts"
  add_foreign_key "order_items", "orders"
  add_foreign_key "order_items", "products"
  add_foreign_key "orders", "contacts"
  add_foreign_key "orders", "currencies"
  add_foreign_key "parameters", "units"
  add_foreign_key "payments", "orders"
  add_foreign_key "photos", "products"
  add_foreign_key "price_lists", "currencies"
  add_foreign_key "price_lists", "users"
  add_foreign_key "prices", "price_lists"
  add_foreign_key "prices", "products"
  add_foreign_key "product_categories", "categories"
  add_foreign_key "product_categories", "products"
  add_foreign_key "product_parameters", "parameters"
  add_foreign_key "product_parameters", "products"
  add_foreign_key "product_tags", "products"
  add_foreign_key "product_tags", "tags"
  add_foreign_key "products", "units"
  add_foreign_key "shippings", "addresses"
  add_foreign_key "shippings", "orders"
  add_foreign_key "user_discounts", "discounts"
  add_foreign_key "user_discounts", "users"
end
