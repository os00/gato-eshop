class CreateCoupons < ActiveRecord::Migration[5.2]
  def change
    create_table :coupons do |t|
      t.string :code
      t.string :description
      t.boolean :is_active, deafult: false
      t.datetime :start_date
      t.datetime :end_date
      t.decimal :absolute_value
      t.decimal :percent_value

      t.timestamps
    end
  end
end
