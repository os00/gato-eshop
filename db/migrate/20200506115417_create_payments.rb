class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.decimal :price
      t.string :number
      t.integer :status
      t.integer :method
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
