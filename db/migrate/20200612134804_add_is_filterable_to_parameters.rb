class AddIsFilterableToParameters < ActiveRecord::Migration[5.2]
  def change
    add_column :parameters, :is_filterable, :boolean
  end
end
