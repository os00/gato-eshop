class AddFilterOrderToParameters < ActiveRecord::Migration[5.2]
  def change
    add_column :parameters, :filter_order, :string
  end
end
