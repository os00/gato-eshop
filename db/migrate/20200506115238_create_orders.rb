class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :number
      t.text :note
      t.date :date
      t.decimal :total
      t.integer :status
      t.references :contact, foreign_key: true
      t.references :currency, foreign_key: true

      t.timestamps
    end
  end
end
