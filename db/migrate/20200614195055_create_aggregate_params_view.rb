class CreateAggregateParamsView < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
      CREATE VIEW aggregate_params AS (
        SELECT products.*, ARRAY_AGG(parameters.name || '=' || product_parameters.value) AS ag_params
        FROM products
        INNER JOIN product_parameters ON product_parameters.product_id = products.id
        INNER JOIN parameters ON product_parameters.parameter_id = parameters.id
        GROUP BY products.id
      )
    SQL
  end

  def down
    execute('DROP VIEW aggregate_params')
  end
end
