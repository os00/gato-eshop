class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.string :brief_description
      t.text :description
      t.references :unit, foreign_key: true

      t.timestamps
    end
  end
end
