class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.date :date
      t.date :due_date
      t.string :number
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
