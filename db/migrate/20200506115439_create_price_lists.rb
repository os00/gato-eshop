class CreatePriceLists < ActiveRecord::Migration[5.2]
  def change
    create_table :price_lists do |t|
      t.string :name
      t.string :description
      t.boolean :is_primary
      t.references :user, foreign_key: true
      t.references :currency, foreign_key: true

      t.timestamps
    end
  end
end
