class CreateDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :discounts do |t|
      t.boolean :is_active, deafult: false
      t.string :description
      t.datetime :start_date
      t.datetime :end_date
      t.decimal :turnover_value
      t.integer :turnover_period
      t.decimal :percent_value

      t.timestamps
    end
  end
end
