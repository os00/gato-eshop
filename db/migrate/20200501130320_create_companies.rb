class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :id_number
      t.string :vat_id_number
      t.references :contact, foreign_key: true

      t.timestamps
    end
  end
end
