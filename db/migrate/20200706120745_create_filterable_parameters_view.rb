class CreateFilterableParametersView < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
      CREATE VIEW filterable_parameters AS (
        SELECT parameters.name, parameters.filter_order AS filterable_parameters
        FROM parameters
        INNER JOIN product_parameters ON product_parameters.parameter_id = parameters.id
        INNER JOIN products ON products.id = product_parameters.product_id
        WHERE parameters.is_filterable = true
        GROUP BY parameters.name, parameters.filter_order
        ORDER BY parameters.filter_order ASC
      )
    SQL
  end

  def down
    execute('DROP VIEW filtrable_parameters')
  end
end
