class CreateParameters < ActiveRecord::Migration[5.2]
  def change
    create_table :parameters do |t|
      t.string :name
      t.references :unit, foreign_key: true

      t.timestamps
    end
  end
end
