class CreateProductParameters < ActiveRecord::Migration[5.2]
  def change
    create_table :product_parameters do |t|
      t.string :value
      t.references :product, foreign_key: true
      t.references :parameter, foreign_key: true

      t.timestamps
    end
  end
end
