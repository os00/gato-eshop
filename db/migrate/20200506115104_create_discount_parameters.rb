class CreateDiscountParameters < ActiveRecord::Migration[5.2]
  def change
    create_table :discount_parameters do |t|
      t.string :value
      t.references :parameter, foreign_key: true
      t.references :discount, foreign_key: true

      t.timestamps
    end
  end
end
