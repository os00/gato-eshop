class CreateShippings < ActiveRecord::Migration[5.2]
  def change
    create_table :shippings do |t|
      t.decimal :price
      t.string :number
      t.integer :status
      t.integer :method
      t.references :address, foreign_key: true
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
