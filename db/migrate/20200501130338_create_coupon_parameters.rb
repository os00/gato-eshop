class CreateCouponParameters < ActiveRecord::Migration[5.2]
  def change
    create_table :coupon_parameters do |t|
      t.string :value
      t.references :coupon, foreign_key: true
      t.references :parameter, foreign_key: true

      t.timestamps
    end
  end
end
