# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require './db/seeds/admin.rb'
require './db/seeds/user.rb'
require './db/seeds/unit.rb'
require './db/seeds/parameter.rb'
require './db/seeds/currency.rb'
require './db/seeds/tag.rb'
require './db/seeds/category.rb'
require './db/seeds/coupon.rb'
require './db/seeds/coupon_parameter.rb'
require './db/seeds/discount.rb'
require './db/seeds/discount_parameter.rb'
require './db/seeds/user_discount.rb'
require './db/seeds/product.rb'
require './db/seeds/photo.rb'
require './db/seeds/product_tag.rb'
require './db/seeds/product_category.rb'
require './db/seeds/product_parameter.rb'
require './db/seeds/price_list.rb'
require './db/seeds/price.rb'
require './db/seeds/contact.rb'
require './db/seeds/company.rb'
require './db/seeds/address.rb'
require './db/seeds/order.rb'
require './db/seeds/order_item.rb'
require './db/seeds/payment.rb'
require './db/seeds/invoice.rb'
require './db/seeds/shipping.rb'

# Seed Admins
print 'Seeding Admins ...'
seed_admins
puts 'OK'

# Seed Users
print 'Seeding Users ...'
users = seed_users
puts 'OK'

# Seed Units
print 'Seeding Units ...'
units = seed_units
puts 'OK'

# Seed Parameters
print 'Seeding Parameters ...'
parameters = seed_parameters(units)
puts 'OK'

# Seed Currencies
print 'Seeding Currencies ...'
currencies = seed_currencies
puts 'OK'

# Seed Tags
print 'Seeding Tags ...'
tags = seed_tags
puts 'OK'

# Seed categories
print 'Seeding Categories ...'
categories = seed_categories
puts 'OK'

# Seed Coupons
print 'Seeding Coupons ...'
coupons = seed_coupons
puts 'OK'

# Seed Discounts
print 'Seeding Discounts ...'
discounts = seed_discounts
puts 'OK'

# Seed User discounts
print 'Seeding User discounts ...'
seed_user_discounts(users, discounts)
puts 'OK'

# Seed Products
print 'Seeding Products ...'
products = seed_products(units)
puts 'OK'

# Seed Photos
print 'Seeding Photos ...'
seed_photos(products)
puts 'OK'

# Seed Product tags
print 'Seeding Product tags ...'
seed_product_tags(tags)
puts 'OK'

# Seed Product categories
print 'Seeding Product categories ...'
seed_product_categories(categories)
puts 'OK'

# Seed Product parameters
print 'Seeding Product parameters ...'
seed_product_parameters(products, parameters)
puts 'OK'

# Seed Coupon parameters
print 'Seeding Coupon parameters ...'
seed_coupon_parameters(coupons, parameters)
puts 'OK'

# Seed Discount parameters
print 'Seeding Discount parameters ...'
seed_discount_parameters(discounts, parameters)
puts 'OK'

# Seed Price lists
print 'Seeding Price lists ...'
price_lists = seed_price_lists(currencies, users)
puts 'OK'

# Seed Prices
print 'Seeding Prices ...'
seed_prices(price_lists, products)
puts 'OK'

# Seed Contacts
print 'Seeding Contacts ...'
contacts = seed_contacts(users)
puts 'OK'

# Seed Companies
print 'Seeding Companies ...'
companies = seed_companies(contacts)
puts 'OK'

# Seed Addresses
print 'Seeding Addresses ...'
seed_addresses(contacts, companies)
puts 'OK'

# Seed Orders
print 'Seeding Orders ...'
orders = seed_orders(currencies, contacts)
puts 'OK'

# Seed Order items
print 'Seeding Order items ...'
seed_order_items(orders, products)
puts 'OK'

# Seed Payments
print 'Seeding Payments ...'
seed_payments(orders)
puts 'OK'

# Seed Invoices
print 'Seeding Invoices ...'
seed_invoices(orders)
puts 'OK'

# Seed Shippings
print 'Seeding Shippings ...'
seed_shippings(orders)
puts 'OK'
