# frozen_string_literal: true

# Seed Products
def seed_photos(products)
  photos = {}
  counter = 0

  products.each do |name, product|
    path = "products/#{counter}/P0#{counter % 3 + 1}"
    photos[name] = []
    photos[name][0] = Photo.create!(
      path: "#{path}-01.jpg",
      description: '',
      order: '0',
      product: product
    )
    photos[name][1] = Photo.create!(
      path: "#{path}-02.jpg",
      description: '',
      order: '1',
      product: product
    )
    photos[name][2] = Photo.create!(
      path: "#{path}-03.jpg",
      description: '',
      order: '2',
      product: product
    )
    counter += 1
  end

  return photos
end
