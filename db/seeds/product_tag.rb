# frozen_string_literal: true

# Seed Product tags
def seed_product_tags(tags)
  selected_products = Product.where('brief_description LIKE ?', '% Al %')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:al])
  end

  selected_products = Product.where('brief_description LIKE ?', '% Fe %')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:fe])
  end

  selected_products = Product.where('brief_description LIKE ?', '%stříbrn%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:silver])
  end

  selected_products = Product.where('brief_description LIKE ?', '%zlat%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:golden])
  end

  selected_products = Product.where('brief_description LIKE ?', '%bíl%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:white])
  end

  selected_products = Product.where('brief_description LIKE ?', '%javor%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:maple])
    ProductTag.create!(product: product, tag: tags[:wooden_decor])
  end

  selected_products = Product.where('brief_description NOT LIKE ?', '%javor%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:basic_color])
  end

  selected_products = Product.where('name LIKE ?', '%HK%')
                             .or(Product.where("name LIKE ?", '%SK%'))
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:rails])
  end

  selected_products = Product.where("name NOT LIKE ? AND name NOT LIKE ?", '%HK%', '%SK%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:door_profiles])
  end

  selected_products = Product.where('name LIKE ?', '%HK%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:hk])
  end

  selected_products = Product.where('name LIKE ?', '%SK%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:sk])
  end

  selected_products = Product.where('name LIKE ?', '%VR%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:vr])
  end

  selected_products = Product.where('name LIKE ?', '%HR%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:hr])
  end

  selected_products = Product.where('name LIKE ?', '%DR%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:dr])
  end

  selected_products = Product.where('(name LIKE ? OR name LIKE ?) AND brief_description LIKE ?', '%HK%', '%SK%', '%Al%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:d10])
    ProductTag.create!(product: product, tag: tags[:d16])
  end

  selected_products = Product.where('(name LIKE ? OR name LIKE ?) AND brief_description LIKE ?', '%HK%', '%SK%', '%Fe%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:d10])
    ProductTag.create!(product: product, tag: tags[:d4])
  end

  selected_products = Product.where('brief_description LIKE ? AND brief_description LIKE ? ', '%10%', '%Al%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:d10])
  end

  selected_products = Product.where('brief_description LIKE ? AND brief_description LIKE ? ', '%16%', '%Al%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:d16])
  end

  selected_products = Product.where('brief_description LIKE ? AND brief_description LIKE ? ', '%10%', '%Fe%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:d10])
  end

  selected_products = Product.where('brief_description LIKE ? AND brief_description LIKE ? ', '%4%', '%Fe%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:d4])
  end

  selected_products = Product.where('name LIKE ? AND brief_description LIKE ?', '%DR%', '%Fe%')
  selected_products.each do |product|
    ProductTag.create!(product: product, tag: tags[:d10])
    ProductTag.create!(product: product, tag: tags[:d4])
  end
end
