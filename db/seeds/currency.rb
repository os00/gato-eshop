# frozen_string_literal: true

# Seed Currencies
def seed_currencies
  currencies = {}
  currencies[:CZK] = Currency.create!(currency: 'CZK')
  currencies[:EUR] = Currency.create!(currency: 'EUR')

  return currencies
end
