# frozen_string_literal: true

# Seed Invoices
def seed_invoices(orders)
  Invoice.create!(
    date: orders[0].date,
    due_date: orders[0].date + 7,
    number: "I#{orders[0].number}",
    order: orders[0]
  )
  Invoice.create!(
    date: orders[1].date,
    due_date: orders[1].date + 7,
    number: "I#{orders[1].number}",
    order: orders[1]
  )
  Invoice.create!(
    date: orders[2].date,
    due_date: orders[2].date + 7,
    number: "I#{orders[2].number}",
    order: orders[2]
  )
  Invoice.create!(
    date: orders[3].date,
    due_date: orders[3].date + 7,
    number: "I#{orders[3].number}",
    order: orders[3]
  )
  Invoice.create!(
    date: orders[4].date,
    due_date: orders[4].date + 7,
    number: "I#{orders[4].number}",
    order: orders[4]
  )
  Invoice.create!(
    date: orders[5].date,
    due_date: orders[5].date + 7,
    number: "I#{orders[5].number}",
    order: orders[5]
  )
  Invoice.create!(
    date: orders[6].date,
    due_date: orders[6].date + 7,
    number: "I#{orders[6].number}",
    order: orders[6]
  )
  Invoice.create!(
    date: orders[7].date,
    due_date: orders[7].date + 7,
    number: "I#{orders[7].number}",
    order: orders[7]
  )
  Invoice.create!(
    date: orders[8].date,
    due_date: orders[8].date + 7,
    number: "I#{orders[8].number}",
    order: orders[8]
  )
  Invoice.create!(
    date: orders[9].date,
    due_date: orders[9].date + 7,
    number: "I#{orders[9].number}",
    order: orders[9]
  )
  Invoice.create!(
    date: orders[10].date,
    due_date: orders[10].date + 7,
    number: "I#{orders[10].number}",
    order: orders[10]
  )
end
