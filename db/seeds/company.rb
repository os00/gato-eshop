# frozen_string_literal: true

# Seed Companies
def seed_companies(contacts)
  companies = []
  companies[0] = Company.create!(
    name: 'GATO spol. s.r.o.',
    id_number: '41186344',
    vat_id_number: 'CZ41186344',
    contact: contacts[1]
  )
  companies[1] = Company.create!(
    name: 'Maděrič & syn s.r.o.',
    id_number: '34233674',
    vat_id_number: 'CZ34233674',
    contact: contacts[2]
  )
  companies[2] = Company.create!(
    name: 'Slavoman a.s.',
    id_number: '22344657',
    vat_id_number: 'CZ22344657',
    contact: contacts[3]
  )

  return companies
end
