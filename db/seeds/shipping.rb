# frozen_string_literal: true

# Seed Shippings
def seed_shippings(orders)
  Shipping.create!(
    price: 247,
    number: "S#{orders[0].number}",
    status: 'processing',
    method: 'toptrans',
    order: orders[0],
    address: orders[0].contact.addresses.first
  )
  Shipping.create!(
    price: 0,
    number: "S#{orders[1].number}",
    status: 'processing',
    method: 'pickup',
    order: orders[1],
    address: orders[1].contact.company.address
  )
  Shipping.create!(
    price: 0,
    number: "S#{orders[2].number}",
    status: 'processing',
    method: 'pickup',
    order: orders[2],
    address: orders[2].contact.addresses.first
  )
  Shipping.create!(
    price: 125,
    number: "S#{orders[3].number}",
    status: 'processing',
    method: 'post',
    order: orders[3],
    address: orders[3].contact.company.address
  )
  Shipping.create!(
    price: 312,
    number: "S#{orders[4].number}",
    status: 'processing',
    method: 'toptrans',
    order: orders[4],
    address: orders[4].contact.company.address
  )
  Shipping.create!(
    price: 197,
    number: "S#{orders[5].number}",
    status: 'ready',
    method: 'toptrans',
    order: orders[5],
    address: orders[5].contact.company.address
  )
  Shipping.create!(
    price: 125,
    number: "S#{orders[6].number}",
    status: 'handed_over',
    method: 'post',
    order: orders[6],
    address: orders[6].contact.addresses.first
  )
  Shipping.create!(
    price: 0,
    number: "S#{orders[7].number}",
    status: 'ready',
    method: 'pickup',
    order: orders[7],
    address: orders[7].contact.addresses.first
  )
  Shipping.create!(
    price: 342,
    number: "S#{orders[8].number}",
    status: 'delivered',
    method: 'toptrans',
    order: orders[8],
    address: orders[8].contact.company.address
  )
  Shipping.create!(
    price: 0,
    number: "S#{orders[9].number}",
    status: 'delivered',
    method: 'pickup',
    order: orders[9],
    address: orders[9].contact.addresses.first
  )
  Shipping.create!(
    price: 269,
    number: "S#{orders[10].number}",
    status: 'delivered',
    method: 'toptrans',
    order: orders[10],
    address: orders[10].contact.addresses.first
  )
end
