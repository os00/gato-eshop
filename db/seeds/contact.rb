# frozen_string_literal: true

# Seed Contacts
def seed_contacts(users)
  contacts = []
  contacts[0] = Contact.create!(
    first_name: 'Tatiana',
    last_name: 'Trecáková',
    phone: '+420777354243',
    email: 'ttrecakova@ayming.com',
    user: users[0]
  )
  contacts[1] = Contact.create!(
    first_name: 'Ondřej',
    last_name: 'Svejkovský',
    phone: '+420776573881',
    email: 'osvejkovsky@ayming.com',
    user: users[1]
  )
  contacts[2] = Contact.create!(
    first_name: 'Jan',
    last_name: 'Slavíček',
    phone: '+420786772993',
    email: 'jslavicek@ayming.com',
    user: users[2]
  )
  contacts[3] = Contact.create!(
    first_name: 'Petr',
    last_name: 'Maděra',
    phone: '+42073737388',
    email: 'pmadera@gmail.com'
  )
  contacts[4] = Contact.create!(
    first_name: 'Josef',
    last_name: 'Jonášek',
    phone: '+420700334576',
    email: 'josef.jonasek@outlook.cz'
  )

  return contacts
end
