# frozen_string_literal: true

# Seed Orders
def seed_orders(currencies, contacts)
  orders = []
  orders[0] = Order.create!(
    number: '123/2020',
    date: Date.today,
    total: 0,
    status: 'received',
    currency: currencies[:CZK],
    contact: contacts[0]
  )
  orders[1] = Order.create!(
    number: '105/2020',
    date: Date.today,
    total: 0,
    status: 'received',
    currency: currencies[:CZK],
    contact: contacts[1]
  )
  orders[2] = Order.create!(
    number: '94/2020',
    date: Date.today - 1,
    total: 0,
    status: 'processing',
    currency: currencies[:CZK],
    contact: contacts[0]
  )
  orders[3] = Order.create!(
    number: '89/2020',
    date: Date.today - 2,
    total: 0,
    status: 'processing',
    currency: currencies[:CZK],
    contact: contacts[1]
  )
  orders[4] = Order.create!(
    number: '70/2020',
    date: Date.today - 4,
    total: 0,
    status: 'processing',
    currency: currencies[:CZK],
    contact: contacts[2]
  )
  orders[5] = Order.create!(
    number: '67/2020',
    date: Date.today - 5,
    total: 0,
    status: 'processing',
    currency: currencies[:CZK],
    contact: contacts[3]
  )
  orders[6] = Order.create!(
    number: '51/2020',
    date: Date.today - 7,
    total: 0,
    status: 'shipped',
    currency: currencies[:CZK],
    contact: contacts[4]
  )
  orders[7] = Order.create!(
    number: '43/2020',
    date: Date.today - 9,
    total: 0,
    status: 'shipped',
    currency: currencies[:CZK],
    contact: contacts[2]
  )
  orders[8] = Order.create!(
    number: '35/2020',
    date: Date.today - 12,
    total: 0,
    status: 'closed',
    currency: currencies[:CZK],
    contact: contacts[3]
  )
  orders[9] = Order.create!(
    number: '14/2020',
    date: Date.today - 22,
    total: 0,
    status: 'closed',
    currency: currencies[:CZK],
    contact: contacts[4]
  )
  orders[10] = Order.create!(
    number: '11/2020',
    date: Date.today - 43,
    total: 0,
    status: 'closed',
    currency: currencies[:CZK],
    contact: contacts[0]
  )

  return orders
end
