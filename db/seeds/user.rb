# frozen_string_literal: true

# Seed user
def seed_users
  users = []
  users[0] = User.new(
    email: 'ttrecakova@ayming.com', password: 'ayming01', is_active: true,
    role: 'undefined'
  )
  users[1] = User.new(
    email: 'osvejkovsky@ayming.com', password: 'ayming01', is_active: true,
    role: 'undefined'
  )
  users[2] = User.new(
    email: 'jslavicek@ayming.com', password: 'ayming01', is_active: false,
    role: 'undefined'
  )

  users.each do |user|
    user.skip_confirmation!
    user.save!
  end

  return users
end
