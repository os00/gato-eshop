# frozen_string_literal: true

# Seed Prices
def seed_prices(price_lists, products)
  # ****************************************************************************
  # ********************** WHOLESALE *******************************************
  # ****************************************************************************

  # Al Silver 10 mm
  Price.create!(price: 435,
                price_list: price_lists[:wholesale],
                product: products[:HK_Al_S])
  Price.create!(price: 277,
                price_list: price_lists[:wholesale],
                product: products[:SK_Al_S])
  Price.create!(price: 195,
                price_list: price_lists[:wholesale],
                product: products[:VRO1_Al_S_10])
  Price.create!(price: 184,
                price_list: price_lists[:wholesale],
                product: products[:VRU_Al_S_10])
  Price.create!(price: 172,
                price_list: price_lists[:wholesale],
                product: products[:HRH_Al_S_10])
  Price.create!(price: 131,
                price_list: price_lists[:wholesale],
                product: products[:HRS_Al_S_10])
  Price.create!(price: 163,
                price_list: price_lists[:wholesale],
                product: products[:DR_Al_S_10])

  # ============================================================================
  # Al Golden 10 mm
  Price.create!(price: 558,
                price_list: price_lists[:wholesale],
                product: products[:HK_Al_Z])
  Price.create!(price: 339,
                price_list: price_lists[:wholesale],
                product: products[:SK_Al_Z])
  Price.create!(price: 256,
                price_list: price_lists[:wholesale],
                product: products[:VRO1_Al_Z_10])
  Price.create!(price: 213,
                price_list: price_lists[:wholesale],
                product: products[:VRU_Al_Z_10])
  Price.create!(price: 205,
                price_list: price_lists[:wholesale],
                product: products[:HRH_Al_Z_10])
  Price.create!(price: 174,
                price_list: price_lists[:wholesale],
                product: products[:HRS_Al_Z_10])
  Price.create!(price: 191,
                price_list: price_lists[:wholesale],
                product: products[:DR_Al_Z_10])

  # ============================================================================
  # Al Silver 16 mm
  Price.create!(price: 236,
                price_list: price_lists[:wholesale],
                product: products[:VRO_Al_S_16])
  Price.create!(price: 190,
                price_list: price_lists[:wholesale],
                product: products[:HRH_Al_S_16])
  Price.create!(price: 159,
                price_list: price_lists[:wholesale],
                product: products[:HRS_Al_S_16])
  Price.create!(price: 170,
                price_list: price_lists[:wholesale],
                product: products[:DR_Al_S_16])

  # ============================================================================
  # Fe Golden 10 & 4 mm
  Price.create!(price: 332,
                price_list: price_lists[:wholesale],
                product: products[:HK_Fe_Z])
  Price.create!(price: 194,
                price_list: price_lists[:wholesale],
                product: products[:SK_Fe_Z])
  Price.create!(price: 99,
                price_list: price_lists[:wholesale],
                product: products[:VR10_Fe_Z])
  Price.create!(price: 123,
                price_list: price_lists[:wholesale],
                product: products[:VR10_Lux_Fe_Z])
  Price.create!(price: 99,
                price_list: price_lists[:wholesale],
                product: products[:VR4_Fe_Z])
  Price.create!(price: 123,
                price_list: price_lists[:wholesale],
                product: products[:VR4_Lux_Fe_Z])
  Price.create!(price: 78,
                price_list: price_lists[:wholesale],
                product: products[:HR10_Fe_Z])
  Price.create!(price: 78,
                price_list: price_lists[:wholesale],
                product: products[:HR4_Fe_Z])
  Price.create!(price: 155,
                price_list: price_lists[:wholesale],
                product: products[:DR10_Fe_Z])

  # ============================================================================
  # Fe White 10 & 4 mm
  Price.create!(price: 332,
                price_list: price_lists[:wholesale],
                product: products[:HK_Fe_B])
  Price.create!(price: 194,
                price_list: price_lists[:wholesale],
                product: products[:SK_Fe_B])
  Price.create!(price: 99,
                price_list: price_lists[:wholesale],
                product: products[:VR10_Fe_B])
  Price.create!(price: 123,
                price_list: price_lists[:wholesale],
                product: products[:VR10_Lux_Fe_B])
  Price.create!(price: 99,
                price_list: price_lists[:wholesale],
                product: products[:VR4_Fe_B])
  Price.create!(price: 123,
                price_list: price_lists[:wholesale],
                product: products[:VR4_Lux_Fe_B])
  Price.create!(price: 78,
                price_list: price_lists[:wholesale],
                product: products[:HR10_Fe_B])
  Price.create!(price: 78,
                price_list: price_lists[:wholesale],
                product: products[:HR4_Fe_B])
  Price.create!(price: 155,
                price_list: price_lists[:wholesale],
                product: products[:DR10_Fe_B])

  # ============================================================================
  # Fe Maple 10 & 4 mm
  Price.create!(price: 448,
                price_list: price_lists[:wholesale],
                product: products[:HK_Fe_Ja])
  Price.create!(price: 254,
                price_list: price_lists[:wholesale],
                product: products[:SK_Fe_Ja])
  Price.create!(price: 139,
                price_list: price_lists[:wholesale],
                product: products[:VR10_Fe_Ja])
  Price.create!(price: 165,
                price_list: price_lists[:wholesale],
                product: products[:VR10_Lux_Fe_Ja])
  Price.create!(price: 139,
                price_list: price_lists[:wholesale],
                product: products[:VR4_Fe_Ja])
  Price.create!(price: 165,
                price_list: price_lists[:wholesale],
                product: products[:VR4_Lux_Fe_Ja])
  Price.create!(price: 111,
                price_list: price_lists[:wholesale],
                product: products[:HR10_Fe_Ja])
  Price.create!(price: 111,
                price_list: price_lists[:wholesale],
                product: products[:HR4_Fe_Ja])
  Price.create!(price: 187,
                price_list: price_lists[:wholesale],
                product: products[:DR10_Fe_Ja])

  # ****************************************************************************
  # ********************** SVEJKOVSKY ******************************************
  # ****************************************************************************

  # Al Silver 10 mm
  Price.create!(price: 399,
                price_list: price_lists[:svejkovsky],
                product: products[:HK_Al_S])
  Price.create!(price: 247,
                price_list: price_lists[:svejkovsky],
                product: products[:SK_Al_S])
  Price.create!(price: 175,
                price_list: price_lists[:svejkovsky],
                product: products[:VRO1_Al_S_10])
  Price.create!(price: 165,
                price_list: price_lists[:svejkovsky],
                product: products[:VRU_Al_S_10])
  Price.create!(price: 155,
                price_list: price_lists[:svejkovsky],
                product: products[:HRH_Al_S_10])
  Price.create!(price: 120,
                price_list: price_lists[:svejkovsky],
                product: products[:HRS_Al_S_10])
  Price.create!(price: 155,
                price_list: price_lists[:svejkovsky],
                product: products[:DR_Al_S_10])

  # ============================================================================
  # Al Golden 10 mm
  Price.create!(price: 499,
                price_list: price_lists[:svejkovsky],
                product: products[:HK_Al_Z])
  Price.create!(price: 299,
                price_list: price_lists[:svejkovsky],
                product: products[:SK_Al_Z])
  Price.create!(price: 229,
                price_list: price_lists[:svejkovsky],
                product: products[:VRO1_Al_Z_10])
  Price.create!(price: 199,
                price_list: price_lists[:svejkovsky],
                product: products[:VRU_Al_Z_10])
  Price.create!(price: 185,
                price_list: price_lists[:svejkovsky],
                product: products[:HRH_Al_Z_10])
  Price.create!(price: 159,
                price_list: price_lists[:svejkovsky],
                product: products[:HRS_Al_Z_10])
  Price.create!(price: 179,
                price_list: price_lists[:svejkovsky],
                product: products[:DR_Al_Z_10])

  # ============================================================================
  # Al Silver 16 mm
  Price.create!(price: 209,
                price_list: price_lists[:svejkovsky],
                product: products[:VRO_Al_S_16])
  Price.create!(price: 175,
                price_list: price_lists[:svejkovsky],
                product: products[:HRH_Al_S_16])
  Price.create!(price: 145,
                price_list: price_lists[:svejkovsky],
                product: products[:HRS_Al_S_16])
  Price.create!(price: 159,
                price_list: price_lists[:svejkovsky],
                product: products[:DR_Al_S_16])
end
