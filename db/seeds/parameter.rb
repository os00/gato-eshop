# frozen_string_literal: true

# Seed Parameters
def seed_parameters(units)
  parameters = {}
  parameters[:length] = Parameter.create!(name: 'Délka', is_filterable: false, filter_order: '7', unit: units[:m])
  parameters[:height] = Parameter.create!(name: 'Výška', is_filterable: false, filter_order: '6', unit: units[:mm])
  parameters[:width] = Parameter.create!(name: 'Šířka', is_filterable: false, filter_order: '5', unit: units[:mm])
  parameters[:color] = Parameter.create!(name: 'Barva', is_filterable: true, filter_order: '3')
  parameters[:material] = Parameter.create!(name: 'Materiál', is_filterable: true, filter_order: '0')
  parameters[:color_type] = Parameter.create!(name: 'Typ barvy', is_filterable: true, filter_order: '2')
  parameters[:groove] = Parameter.create!(name: 'Drážka', unit: units[:mm], is_filterable: true, filter_order: '4')
  parameters[:profile_type] = Parameter.create!(name: 'Typ profilu', is_filterable: true, filter_order: '1')

  return parameters
end
