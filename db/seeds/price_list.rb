# frozen_string_literal: true

# Seed Price lists
def seed_price_lists(currencies, users)
  price_lists = {}
  price_lists[:wholesale] = PriceList.create!(
    name: 'Velkoobchod',
    description: 'Všeobecný velkoobchodní ceník',
    currency: currencies[:CZK],
    is_primary: true
  )
  price_lists[:svejkovsky] = PriceList.create!(
    name: 'Ceník Svejkovský',
    description: 'Speciální ceník pro věrného zákaníka Ondřeje
                  Svejkovského',
    user: users[1],
    currency: currencies[:CZK],
    is_primary: false
  )

  return price_lists
end
