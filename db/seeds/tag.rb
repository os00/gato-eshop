# frozen_string_literal: true

# Seed Tags
def seed_tags
  tags = {}
  tags[:silver] = Tag.create!(name: 'Stříbrná')
  tags[:golden] = Tag.create!(name: 'Zlatá')
  tags[:white] = Tag.create!(name: 'Bílá')
  tags[:cherry] = Tag.create!(name: 'Třešeň')
  tags[:maple] = Tag.create!(name: 'Javor')
  tags[:wooden_decor] = Tag.create!(name: 'Dřevodekor')
  tags[:basic_color] = Tag.create!(name: 'Základní barva')
  tags[:al] = Tag.create!(name: 'Hliník')
  tags[:fe] = Tag.create!(name: 'Ocel')
  tags[:rails] = Tag.create!(name: 'Kolej')
  tags[:door_profiles] = Tag.create!(name: 'Dveřní profil')
  tags[:hk] = Tag.create!(name: 'Horní kolej')
  tags[:sk] = Tag.create!(name: 'Spodní kolej')
  tags[:vr] = Tag.create!(name: 'Vertikála')
  tags[:hr] = Tag.create!(name: 'Horizontála')
  tags[:dr] = Tag.create!(name: 'Dělička')
  tags[:d10] = Tag.create!(name: '10 mm')
  tags[:d16] = Tag.create!(name: '16 mm')
  tags[:d4] = Tag.create!(name: '4 mm')
  tags[:additional_material] = Tag.create!(name: 'Doplňkový materiál')
  tags[:wheels] = Tag.create!(name: 'Pojezd')
  tags[:brushes] = Tag.create!(name: 'Kartáček')
  tags[:screws] = Tag.create!(name: 'Šrouby')
  tags[:brakes] = Tag.create!(name: 'Brzda')

  return tags
end
