# frozen_string_literal: true

# Seed Product categories
def seed_product_categories(categories)
  selected_products = Product.where('brief_description LIKE ?', '% Al %')
  selected_products.each do |product|
    ProductCategory.create!(product: product, category: categories[:al])
  end

  selected_products = Product.where('brief_description LIKE ?', '%javor%')
  selected_products.each do |product|
    ProductCategory.create!(product: product, category: categories[:wooden_decor])
  end

  selected_products = Product.where('brief_description LIKE ? AND brief_description NOT LIKE ?', '%Fe%', '%javor%')
  selected_products.each do |product|
    ProductCategory.create!(product: product, category: categories[:basic_color])
  end
end
