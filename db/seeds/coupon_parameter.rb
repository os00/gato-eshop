# frozen_string_literal: true

# Seed Coupon categories
def seed_coupon_parameters(coupons, parameters)
  CouponParameter.create!(coupon: coupons[0],
                          parameter: parameters[:color],
                          value: 'Stříbrná')
  CouponParameter.create!(coupon: coupons[1],
                          parameter: parameters[:color],
                          value: 'Bílá')
  CouponParameter.create!(coupon: coupons[1],
                          parameter: parameters[:material],
                          value: 'Ocel')
  CouponParameter.create!(coupon: coupons[3],
                          parameter: parameters[:color],
                          value: 'Zlatá')
  CouponParameter.create!(coupon: coupons[3],
                          parameter: parameters[:material],
                          value: 'Hliník')
  CouponParameter.create!(coupon: coupons[5],
                          parameter: parameters[:color_type],
                          value: 'Základní barva')
  CouponParameter.create!(coupon: coupons[5],
                          parameter: parameters[:material],
                          value: 'Ocel')
end
