# frozen_string_literal: true

# Seed Order items
def seed_order_items(orders, products)
  orders.each do |order|
    rand(2..13).times do
      product = products[products.keys.sample]
      OrderItem.create!(
        quantity: rand(1..7),
        price: find_price(product, order.contact),
        order: order,
        product: product
      )
    end
  end
end

def find_price(product, contact)
  price = 0
  primary_price_list = PriceList.where(is_primary: true).first

  # If contact is not registered (no user) or contact has no custom price list
  # or custom price list has no price for given product
  if contact.user.nil? ||
     contact.user.price_list.nil? ||
     contact.user.price_list.prices.where(product: product).count.zero?
    price = product.prices.where(price_list: primary_price_list).first.price
  else # custom price list exists and has price for given product
    user_price_list = contact.user.price_list
    price = product.prices.where(price_list: user_price_list).first.price
  end

  return calculate_discount(price, contact)
end

def calculate_discount(price, contact)
  return price if contact.user.nil? || contact.user.discounts.count.zero?

  contact.user.discounts.each do |discount|
    price = price * (100 - discount.percent_value) / 100
  end

  return price
end
