# frozen_string_literal: true

# Seed User discounts
def seed_user_discounts(users, discounts)
  UserDiscount.create!(user: users[0], discount: discounts[0])
  UserDiscount.create!(user: users[0], discount: discounts[1])
  UserDiscount.create!(user: users[1], discount: discounts[3])
  UserDiscount.create!(user: users[2], discount: discounts[4])
end
