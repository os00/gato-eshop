# frozen_string_literal: true

# Seed admin
def seed_admins
  Admin.create!(email: 'os00@outlook.cz', password: 'zarubova', is_active: true)
end
