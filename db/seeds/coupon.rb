# frozen_string_literal: true

# Seed Coupons
def seed_coupons
  date_now = Date.today
  coupons = []
  coupons[0] = Coupon.create!(
    code: 'a53f0kl5df3m', description: 'XMas coupons', is_active: true,
    start_date: date_now, end_date: date_now + 365,
    absolute_value: 1000,
    percent_value: 0
  )
  coupons[1] = Coupon.create!(
    code: 'j2k75mmd9032', description: 'XMas coupons', is_active: true,
    start_date: date_now - 50, end_date: date_now - 1,
    absolute_value: 0,
    percent_value: 25
  )
  coupons[2] = Coupon.create!(
    code: '00kzxo263mgl', description: 'XMas coupons', is_active: true,
    absolute_value: 2,
    percent_value: 0
  )
  coupons[3] = Coupon.create!(
    code: 'h804mxppf81b', description: 'XMas coupons', is_active: true,
    absolute_value: 1500,
    percent_value: 0
  )
  coupons[4] = Coupon.create!(
    code: 'wiur863320xm', description: 'XMas coupons', is_active: false,
    absolute_value: 0,
    percent_value: 5
  )
  coupons[5] = Coupon.create!(
    code: 'aslbt840dbc8', description: 'XMas coupons', is_active: false,
    start_date: date_now, end_date: date_now + 365,
    absolute_value: 0,
    percent_value: 10
  )

  return coupons
end
