# frozen_string_literal: true

# Seed Products
def seed_products(units)
  products = {}

  # Al Silver 10 mm
  products[:HK_Al_S] = Product.create!(
    name: 'HK', unit: units[:ks],
    brief_description: 'Horní kolej Al stříbrná',
    description: 'Horní kolej z extrudovaného hliníku stříbrné
                  barvy. V koleji pojíždějí dveře vestavěné skříně.'
  )
  products[:SK_Al_S] = Product.create!(
    name: 'SK', unit: units[:ks],
    brief_description: 'Spodní kolej Al stříbrná',
    description: 'Spodní kolej z extrudovaného hliníku stříbrné
                  barvy. V koleji pojíždějí dveře vestavěné skříně.'
  )
  products[:VRO1_Al_S_10] = Product.create!(
    name: 'VRO1', unit: units[:ks],
    brief_description: 'Vertikální profil otevřený Al stříbrný, 10 mm',
    description: 'Vertikální profil otevřený z extrudovaného
                  hliníku stříbrné barvy pro lamina tloušťky 10 mm a zrcadla.
                  Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil
                  uzavřený (VRU)'
  )
  products[:VRU_Al_S_10] = Product.create!(
    name: 'VRU', unit: units[:ks],
    brief_description: 'Vertikální profil uzavřený Al stříbrný, 10 mm',
    description: 'Vertikální profil uzavřený z extrudovaného
                  hliníku stříbrné barvy pro lamina tloušťky 10 mm a zrcadla.
                  Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil
                  uzavřený (VRO)'
  )
  products[:HRH_Al_S_10] = Product.create!(
    name: 'HRH', unit: units[:ks],
    brief_description: 'Horizontální profil horní Al stříbrný, 10 mm',
    description: 'Horizontální profil horní z extrudovaného
                  hliníku stříbrné barvy pro lamina tloušťky 10 mm a zrcadla.
                  Využívá se jako rám dveří vestavěné
                  skříně.'
  )
  products[:HRS_Al_S_10] = Product.create!(
    name: 'HRS', unit: units[:ks],
    brief_description: 'Horizontální profil spodní Al stříbrný, 10 mm',
    description: 'Horizontální profil spodní z extrudovaného
                  hliníku stříbrné barvy pro lamina tloušťky 10 mm a zrcadla.
                  Využívá se jako rám dveří vestavěné
                  skříně.'
  )
  products[:DR_Al_S_10] = Product.create!(
    name: 'DR', unit: units[:ks],
    brief_description: 'Dělící profil Al stříbrný, 10 mm',
    description: 'Dělící profil z extrudovaného
                  hliníku stříbrné barvy pro lamina tloušťky 10 mm a zrcadla.
                  Využívá se jako dělící příčka rámu
                  dveří vestavěné skříně. Dveře pak mohou mít více polí z
                  různých druhů lamin nebo zrcadel'
  )

  # ============================================================================
  # Al Golden 10 mm
  products[:HK_Al_Z] = Product.create!(
    name: 'HK', unit: units[:ks],
    brief_description: 'Horní kolej Al zlatá',
    description: 'Horní kolej z extrudovaného hliníku zlaté
                  barvy. V koleji pojíždějí dveře vestavěné skříně.'
  )
  products[:SK_Al_Z] = Product.create!(
    name: 'SK', unit: units[:ks],
    brief_description: 'Spodní kolej Al zlatá',
    description: 'Spodní kolej z extrudovaného hliníku zlaté
                  barvy. V koleji pojíždějí dveře vestavěné skříně.'
  )
  products[:VRO1_Al_Z_10] = Product.create!(
    name: 'VRO1', unit: units[:ks],
    brief_description: 'Vertikální profil otevřený Al zlatý, 10 mm',
    description: 'Vertikální profil otevřený z extrudovaného
                  hliníku zlaté barvy pro lamina tloušťky 10 mm a zrcadla.
                  Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil
                  uzavřený (VRU)'
  )
  products[:VRU_Al_Z_10] = Product.create!(
    name: 'VRU', unit: units[:ks],
    brief_description: 'Vertikální profil uzavřený Al zlatý, 10 mm',
    description: 'Vertikální profil uzavřený z extrudovaného
                  hliníku zlaté barvy pro lamina tloušťky 10 mm a zrcadla.
                  Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil
                  uzavřený (VRO)'
  )
  products[:HRH_Al_Z_10] = Product.create!(
    name: 'HRH', unit: units[:ks],
    brief_description: 'Horizontální profil horní Al zlatý, 10 mm',
    description: 'Horizontální profil horní z extrudovaného
                  hliníku zlaté barvy pro lamina tloušťky 10 mm a zrcadla.
                  Využívá se jako rám dveří vestavěné
                  skříně.'
  )
  products[:HRS_Al_Z_10] = Product.create!(
    name: 'HRS', unit: units[:ks],
    brief_description: 'Horizontální profil spodní Al zlatý, 10 mm',
    description: 'Horizontální profil spodní z extrudovaného
                  hliníku zlaté barvy pro lamina tloušťky 10 mm a zrcadla.
                  Využívá se jako rám dveří vestavěné
                  skříně.'
  )
  products[:DR_Al_Z_10] = Product.create!(
    name: 'DR', unit: units[:ks],
    brief_description: 'Dělící profil Al zlatý, 10 mm',
    description: 'Dělící profilům z extrudovaného
                  hliníku zlaté barvy pro lamina tloušťky 10 mm a zrcadla.
                  Využívá se jako dělící příčka rámu
                  dveří vestavěné skříně. Dveře pak mohou mít více polí z
                  různých druhů lamin nebo zrcadel'
  )

  # ============================================================================
  # Al Silver 16 mm
  products[:VRO_Al_S_16] = Product.create!(
    name: 'VRO 16', unit: units[:ks],
    brief_description: 'Vertikální profil otevřený Al stříbrný, 16 mm',
    description: 'Vertikální profil otevřený z extrudovaného
                  hliníku stříbrné barvy pro lamina tloušťky 16 mm.
                  Využívá se jako rám dveří vestavěné
                  skříně.'
  )
  products[:HRH_Al_S_16] = Product.create!(
    name: 'HRH 16', unit: units[:ks],
    brief_description: 'Horizontální profil horní Al stříbrný, 16 mm',
    description: 'Horizontální profil horní z extrudovaného
                  hliníku stříbrné barvy pro lamina tloušťky 16 mm.
                  Využívá se jako rám dveří vestavěné
                  skříně.'
  )
  products[:HRS_Al_S_16] = Product.create!(
    name: 'HRS 16', unit: units[:ks],
    brief_description: 'Horizontální profil spodní Al stříbrný, 16 mm',
    description: 'Horizontální profil spodní z extrudovaného
                  hliníku stříbrné barvy pro lamina tloušťky 16 mm a zrcadla.
                  Využívá se jako rám dveří vestavěné
                  skříně.'
  )
  products[:DR_Al_S_16] = Product.create!(
    name: 'DR 16', unit: units[:ks],
    brief_description: 'Dělící profil Al stříbrný, 16 mm',
    description: 'Dělící profil z extrudovaného
                  hliníku stříbrné barvy pro lamina tloušťky 16 mm.
                  Využívá se jako dělící příčka rámu
                  dveří vestavěné skříně. Dveře pak mohou mít více polí z
                  různých druhů lamin nebo zrcadel'
  )

  # ============================================================================
  # Fe Golden 10 & 4 mm
  products[:HK_Fe_Z] = Product.create!(
    name: 'HK', unit: units[:ks],
    brief_description: 'Horní kolej Fe zlatá',
    description: 'Horní kolej z oceli zlaté
                  barvy. V koleji pojíždějí dveře vestavěné skříně.'
  )
  products[:SK_Fe_Z] = Product.create!(
    name: 'SK', unit: units[:ks],
    brief_description: 'Spodní kolej Fe zlatá',
    description: 'Spodní kolej z oceli zlaté
                  barvy. V koleji pojíždějí dveře vestavěné skříně.'
  )
  products[:VR10_Fe_Z] = Product.create!(
    name: 'VR10', unit: units[:ks],
    brief_description: 'Vertikální profil standard Fe zlatý, 10 mm',
    description: 'Vertikální profil standardní z válcované oceli
                  zlaté barvy pro lamina tloušťky 10 mm.
                  Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil
                  lux (VR10 Lux) nebo profily pro zrcadlo (VR4 a VR4 Lux)'
  )
  products[:VR10_Lux_Fe_Z] = Product.create!(
    name: 'VR10 Lux', unit: units[:ks],
    brief_description: 'Vertikální profil lux Fe zlatý, 10 mm',
    description: 'Vertikální profil luxusní z válcované oceli
                  zlaté barvy pro lamina tloušťky 10 mm. Využívá se jako rám
                  dveří vestavěné skříně.
                  Místo tohoto profilu je možné použít také profil
                  standard (VR10) nebo profily pro zrcadlo (VR4 a VR4 Lux)'
  )
  products[:VR4_Fe_Z] = Product.create!(
    name: 'VR4', unit: units[:ks],
    brief_description: 'Vertikální profil standard Fe zlatý, 4 mm',
    description: 'Vertikální profil standardní z válcované oceli
                  zlaté barvy pro zrcadla tloušťky 4 mm.
                  Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil
                  lux (VR4 Lux) nebo profily pro lamino (VR10 a VR10 Lux)'
  )
  products[:VR4_Lux_Fe_Z] = Product.create!(
    name: 'VR4 Lux', unit: units[:ks],
    brief_description: 'Vertikální profil lux Fe zlatý, 4 mm',
    description: 'Vertikální profil luxusní z válcované oceli
                  zlaté barvy pro zrcadla tloušťky 4 mm. Využívá se jako rám
                  dveří vestavěné skříně.
                  Místo tohoto profilu je možné použít také profil
                  standard (VR4) nebo profily pro lamino (VR10 a VR10 Lux)'
  )
  products[:HR10_Fe_Z] = Product.create!(
    name: 'HR10', unit: units[:ks],
    brief_description: 'Horizontální profil Fe zlatý, 10 mm',
    description: 'Horizontální profil z válcované oceli zlaté barvy
                  pro lamina tloušťky 10 mm. Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil pro
                  zrcadlo (HR4)'
  )
  products[:HR4_Fe_Z] = Product.create!(
    name: 'HR4', unit: units[:ks],
    brief_description: 'Horizontální profil Fe zlatý, 4 mm',
    description: 'Horizontální profil z válcované oceli zlaté barvy
                  pro zrcadla tloušťky 4 mm. Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil pro
                  lamino (HR10)'
  )
  products[:DR10_Fe_Z] = Product.create!(
    name: 'DR10', unit: units[:ks],
    brief_description: 'Dělící profil Fe zlatý',
    description: 'Dělící profil z válcované oceli zlaté barvy pro
                  lamina tloušťky 10 mm nebo zrcadla. Využívá se jako dělící
                  příčka rámu dveří vestavěné skříně. Dveře pak mohou mít více
                  polí z různých druhů lamin nebo zrcadel. V případě použití
                  zrcadla, je nutné použít vložku.'
  )

  # ============================================================================
  # Fe White 10 & 4 mm
  products[:HK_Fe_B] = Product.create!(
    name: 'HK', unit: units[:ks],
    brief_description: 'Horní kolej Fe bílá',
    description: 'Horní kolej z oceli bílé
                  barvy. V koleji pojíždějí dveře vestavěné skříně.'
  )
  products[:SK_Fe_B] = Product.create!(
    name: 'SK', unit: units[:ks],
    brief_description: 'Spodní kolej Fe bílá',
    description: 'Spodní kolej z oceli bílé
                  barvy. V koleji pojíždějí dveře vestavěné skříně.'
  )
  products[:VR10_Fe_B] = Product.create!(
    name: 'VR10', unit: units[:ks],
    brief_description: 'Vertikální profil standard Fe bílý, 10 mm',
    description: 'Vertikální profil standardní z válcované oceli
                  bílé barvy pro lamina tloušťky 10 mm.
                  Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil
                  lux (VR10 Lux) nebo profily pro zrcadlo (VR4 a VR4 Lux)'
  )
  products[:VR10_Lux_Fe_B] = Product.create!(
    name: 'VR10 Lux', unit: units[:ks],
    brief_description: 'Vertikální profil lux Fe bílý, 10 mm',
    description: 'Vertikální profil luxusní z válcované oceli
                  bílé barvy pro lamina tloušťky 10 mm. Využívá se jako rám
                  dveří vestavěné skříně.
                  Místo tohoto profilu je možné použít také profil
                  standard (VR10) nebo profily pro zrcadlo (VR4 a VR4 Lux)'
  )
  products[:VR4_Fe_B] = Product.create!(
    name: 'VR4', unit: units[:ks],
    brief_description: 'Vertikální profil standard Fe bílý, 4 mm',
    description: 'Vertikální profil standardní z válcované oceli
                  bílé barvy pro zrcadla tloušťky 4 mm.
                  Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil
                  lux (VR4 Lux) nebo profily pro lamino (VR10 a VR10 Lux)'
  )
  products[:VR4_Lux_Fe_B] = Product.create!(
    name: 'VR4 Lux', unit: units[:ks],
    brief_description: 'Vertikální profil lux Fe bílý, 4 mm',
    description: 'Vertikální profil luxusní z válcované oceli
                  bílé barvy pro zrcadla tloušťky 4 mm. Využívá se jako rám
                  dveří vestavěné skříně.
                  Místo tohoto profilu je možné použít také profil
                  standard (VR4) nebo profily pro lamino (VR10 a VR10 Lux)'
  )
  products[:HR10_Fe_B] = Product.create!(
    name: 'HR10', unit: units[:ks],
    brief_description: 'Horizontální profil Fe bílý, 10 mm',
    description: 'Horizontální profil z válcované oceli bílé barvy
                  pro lamina tloušťky 10 mm. Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil pro
                  zrcadlo (HR4)'
  )
  products[:HR4_Fe_B] = Product.create!(
    name: 'HR4', unit: units[:ks],
    brief_description: 'Horizontální profil Fe bílý, 4 mm',
    description: 'Horizontální profil z válcované oceli bílé barvy
                  pro zrcadla tloušťky 4 mm. Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil pro
                  lamino (HR10)'
  )
  products[:DR10_Fe_B] = Product.create!(
    name: 'DR10', unit: units[:ks],
    brief_description: 'Dělící profil Fe bílý',
    description: 'Dělící profil z válcované oceli bílé barvy pro
                  lamina tloušťky 10 mm nebo zrcadla. Využívá se jako dělící
                  příčka rámu dveří vestavěné skříně. Dveře pak mohou mít více
                  polí z různých druhů lamin nebo zrcadel. V případě použití
                  zrcadla, je nutné použít vložku.'
  )

  # ============================================================================
  # Fe Maple 10 & 4 mm
  products[:HK_Fe_Ja] = Product.create!(
    name: 'HK', unit: units[:ks],
    brief_description: 'Horní kolej Fe javor',
    description: 'Horní kolej z válcované oceli s javorovým dřevodekorem.
                  V koleji pojíždějí dveře vestavěné skříně.'
  )
  products[:SK_Fe_Ja] = Product.create!(
    name: 'SK', unit: units[:ks],
    brief_description: 'Spodní kolej Fe bílá',
    description: 'Spodní kolej z válcované oceli s javorovým dřevodekorem.
                  V koleji pojíždějí dveře vestavěné skříně.'
  )
  products[:VR10_Fe_Ja] = Product.create!(
    name: 'VR10', unit: units[:ks],
    brief_description: 'Vertikální profil standard Fe javor, 10 mm',
    description: 'Vertikální profil standardní z válcované oceli
                  s javorovým dřevodekorem pro lamina tloušťky 10 mm.
                  Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil
                  lux (VR10 Lux) nebo profily pro zrcadlo (VR4 a VR4 Lux)'
  )
  products[:VR10_Lux_Fe_Ja] = Product.create!(
    name: 'VR10 Lux', unit: units[:ks],
    brief_description: 'Vertikální profil lux Fe javor, 10 mm',
    description: 'Vertikální profil luxusní z válcované oceli
                  s javorovým dřevodekorem pro lamina tloušťky 10 mm. Využívá se jako rám
                  dveří vestavěné skříně.
                  Místo tohoto profilu je možné použít také profil
                  standard (VR10) nebo profily pro zrcadlo (VR4 a VR4 Lux)'
  )
  products[:VR4_Fe_Ja] = Product.create!(
    name: 'VR4', unit: units[:ks],
    brief_description: 'Vertikální profil standard Fe javor, 4 mm',
    description: 'Vertikální profil standardní z válcované oceli
                  s javorovým dřevodekorem pro zrcadla tloušťky 4 mm.
                  Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil
                  lux (VR4 Lux) nebo profily pro lamino (VR10 a VR10 Lux)'
  )
  products[:VR4_Lux_Fe_Ja] = Product.create!(
    name: 'VR4 Lux', unit: units[:ks],
    brief_description: 'Vertikální profil lux Fe javor, 4 mm',
    description: 'Vertikální profil luxusní z válcované oceli
                  s javorovým dřevodekorem pro zrcadla tloušťky 4 mm. Využívá se jako rám
                  dveří vestavěné skříně.
                  Místo tohoto profilu je možné použít také profil
                  standard (VR4) nebo profily pro lamino (VR10 a VR10 Lux)'
  )
  products[:HR10_Fe_Ja] = Product.create!(
    name: 'HR10', unit: units[:ks],
    brief_description: 'Horizontální profil Fe javor, 10 mm',
    description: 'Horizontální profil z válcované oceli s javorovým dřevodekorem
                  pro lamina tloušťky 10 mm. Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil pro
                  zrcadlo (HR4)'
  )
  products[:HR4_Fe_Ja] = Product.create!(
    name: 'HR4', unit: units[:ks],
    brief_description: 'Horizontální profil Fe javor, 4 mm',
    description: 'Horizontální profil z válcované oceli s javorovým dřevodekorem
                  pro zrcadla tloušťky 4 mm. Využívá se jako rám dveří vestavěné
                  skříně. Místo tohoto profilu je možné použít také profil pro
                  lamino (HR10)'
  )
  products[:DR10_Fe_Ja] = Product.create!(
    name: 'DR10', unit: units[:ks],
    brief_description: 'Dělící profil Fe javor',
    description: 'Dělící profil z válcované oceli s javorovým dřevodekorem pro
                  lamina tloušťky 10 mm nebo zrcadla. Využívá se jako dělící
                  příčka rámu dveří vestavěné skříně. Dveře pak mohou mít více
                  polí z různých druhů lamin nebo zrcadel. V případě použití
                  zrcadla, je nutné použít vložku.'
  )

  return products
end
