# frozen_string_literal: true

# Seed Product parameters
def seed_product_parameters(products, parameters)

  # AL =========================================================================
  # HK, Al, Silver
  ProductParameter.create!(product: products[:HK_Al_S],
                           parameter: parameters[:profile_type],
                           value: 'HK')
  ProductParameter.create!(product: products[:HK_Al_S],
                           parameter: parameters[:length],
                           value: '5')
  ProductParameter.create!(product: products[:HK_Al_S],
                           parameter: parameters[:height],
                           value: '50')
  ProductParameter.create!(product: products[:HK_Al_S],
                           parameter: parameters[:width],
                           value: '112')
  ProductParameter.create!(product: products[:HK_Al_S],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:HK_Al_S],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:HK_Al_S],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HK_Al_S],
                           parameter: parameters[:groove],
                           value: '10')
  ProductParameter.create!(product: products[:HK_Al_S],
                           parameter: parameters[:groove],
                           value: '16')

  # SK, Al, Silver
  ProductParameter.create!(product: products[:SK_Al_S],
                           parameter: parameters[:profile_type],
                           value: 'SK')
  ProductParameter.create!(product: products[:SK_Al_S],
                           parameter: parameters[:length],
                           value: '5')
  ProductParameter.create!(product: products[:SK_Al_S],
                           parameter: parameters[:height],
                           value: '12')
  ProductParameter.create!(product: products[:SK_Al_S],
                           parameter: parameters[:width],
                           value: '108')
  ProductParameter.create!(product: products[:SK_Al_S],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:SK_Al_S],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:SK_Al_S],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:SK_Al_S],
                           parameter: parameters[:groove],
                           value: '10')
  ProductParameter.create!(product: products[:SK_Al_S],
                           parameter: parameters[:groove],
                           value: '16')

  # VRO1, Al, Silver, 10 mm
  ProductParameter.create!(product: products[:VRO1_Al_S_10],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VRO1_Al_S_10],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VRO1_Al_S_10],
                           parameter: parameters[:height],
                           value: '36')
  ProductParameter.create!(product: products[:VRO1_Al_S_10],
                           parameter: parameters[:width],
                           value: '32')
  ProductParameter.create!(product: products[:VRO1_Al_S_10],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:VRO1_Al_S_10],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:VRO1_Al_S_10],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VRO1_Al_S_10],
                           parameter: parameters[:groove],
                           value: '10')

  # VRU, Al, Silver, 10 mm
  ProductParameter.create!(product: products[:VRU_Al_S_10],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VRU_Al_S_10],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VRU_Al_S_10],
                           parameter: parameters[:height],
                           value: '38')
  ProductParameter.create!(product: products[:VRU_Al_S_10],
                           parameter: parameters[:width],
                           value: '29')
  ProductParameter.create!(product: products[:VRU_Al_S_10],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:VRU_Al_S_10],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:VRU_Al_S_10],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VRU_Al_S_10],
                           parameter: parameters[:groove],
                           value: '10')

  # HRH, Al, Silver, 10 mm
  ProductParameter.create!(product: products[:HRH_Al_S_10],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HRH_Al_S_10],
                           parameter: parameters[:length],
                           value: '3')
  ProductParameter.create!(product: products[:HRH_Al_S_10],
                           parameter: parameters[:height],
                           value: '75')
  ProductParameter.create!(product: products[:HRH_Al_S_10],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:HRH_Al_S_10],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:HRH_Al_S_10],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:HRH_Al_S_10],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HRH_Al_S_10],
                           parameter: parameters[:groove],
                           value: '10')

  # HRS, Al, Silver, 10 mm
  ProductParameter.create!(product: products[:HRS_Al_S_10],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HRS_Al_S_10],
                           parameter: parameters[:length],
                           value: '3')
  ProductParameter.create!(product: products[:HRS_Al_S_10],
                           parameter: parameters[:height],
                           value: '41')
  ProductParameter.create!(product: products[:HRS_Al_S_10],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:HRS_Al_S_10],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:HRS_Al_S_10],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:HRS_Al_S_10],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HRS_Al_S_10],
                           parameter: parameters[:groove],
                           value: '10')

  # DR, Al, Silver, 10 mm
  ProductParameter.create!(product: products[:DR_Al_S_10],
                           parameter: parameters[:profile_type],
                           value: 'DR')
  ProductParameter.create!(product: products[:DR_Al_S_10],
                           parameter: parameters[:length],
                           value: '3')
  ProductParameter.create!(product: products[:DR_Al_S_10],
                           parameter: parameters[:height],
                           value: '18')
  ProductParameter.create!(product: products[:DR_Al_S_10],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:DR_Al_S_10],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:DR_Al_S_10],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:DR_Al_S_10],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:DR_Al_S_10],
                           parameter: parameters[:groove],
                           value: '10')

  # HK, Al, Gold
  ProductParameter.create!(product: products[:HK_Al_Z],
                           parameter: parameters[:profile_type],
                           value: 'HK')
  ProductParameter.create!(product: products[:HK_Al_Z],
                           parameter: parameters[:length],
                           value: '5')
  ProductParameter.create!(product: products[:HK_Al_Z],
                           parameter: parameters[:height],
                           value: '50')
  ProductParameter.create!(product: products[:HK_Al_Z],
                           parameter: parameters[:width],
                           value: '112')
  ProductParameter.create!(product: products[:HK_Al_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:HK_Al_Z],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:HK_Al_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HK_Al_Z],
                           parameter: parameters[:groove],
                           value: '10')
  ProductParameter.create!(product: products[:HK_Al_Z],
                           parameter: parameters[:groove],
                           value: '16')

  # SK, Al, Gold
  ProductParameter.create!(product: products[:SK_Al_Z],
                           parameter: parameters[:profile_type],
                           value: 'SK')
  ProductParameter.create!(product: products[:SK_Al_Z],
                           parameter: parameters[:length],
                           value: '5')
  ProductParameter.create!(product: products[:SK_Al_Z],
                           parameter: parameters[:height],
                           value: '12')
  ProductParameter.create!(product: products[:SK_Al_Z],
                           parameter: parameters[:width],
                           value: '108')
  ProductParameter.create!(product: products[:SK_Al_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:SK_Al_Z],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:SK_Al_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:SK_Al_Z],
                           parameter: parameters[:groove],
                           value: '10')
  ProductParameter.create!(product: products[:SK_Al_Z],
                           parameter: parameters[:groove],
                           value: '16')

  # VRO1, Al, Gold, 10 mm
  ProductParameter.create!(product: products[:VRO1_Al_Z_10],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VRO1_Al_Z_10],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VRO1_Al_Z_10],
                           parameter: parameters[:height],
                           value: '36')
  ProductParameter.create!(product: products[:VRO1_Al_Z_10],
                           parameter: parameters[:width],
                           value: '32')
  ProductParameter.create!(product: products[:VRO1_Al_Z_10],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:VRO1_Al_Z_10],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:VRO1_Al_Z_10],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VRO1_Al_Z_10],
                           parameter: parameters[:groove],
                           value: '10')

  # VRU, Al, Gold, 10 mm
  ProductParameter.create!(product: products[:VRU_Al_Z_10],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VRU_Al_Z_10],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VRU_Al_Z_10],
                           parameter: parameters[:height],
                           value: '38')
  ProductParameter.create!(product: products[:VRU_Al_Z_10],
                           parameter: parameters[:width],
                           value: '29')
  ProductParameter.create!(product: products[:VRU_Al_Z_10],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:VRU_Al_Z_10],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:VRU_Al_Z_10],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VRU_Al_Z_10],
                           parameter: parameters[:groove],
                           value: '10')

  # HRH, Al, Gold, 10 mm
  ProductParameter.create!(product: products[:HRH_Al_Z_10],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HRH_Al_Z_10],
                           parameter: parameters[:length],
                           value: '3')
  ProductParameter.create!(product: products[:HRH_Al_Z_10],
                           parameter: parameters[:height],
                           value: '75')
  ProductParameter.create!(product: products[:HRH_Al_Z_10],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:HRH_Al_Z_10],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:HRH_Al_Z_10],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:HRH_Al_Z_10],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HRH_Al_Z_10],
                           parameter: parameters[:groove],
                           value: '10')

  # HRS, Al, Gold, 10 mm
  ProductParameter.create!(product: products[:HRS_Al_Z_10],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HRS_Al_Z_10],
                           parameter: parameters[:length],
                           value: '3')
  ProductParameter.create!(product: products[:HRS_Al_Z_10],
                           parameter: parameters[:height],
                           value: '41')
  ProductParameter.create!(product: products[:HRS_Al_Z_10],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:HRS_Al_Z_10],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:HRS_Al_Z_10],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:HRS_Al_Z_10],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HRS_Al_Z_10],
                           parameter: parameters[:groove],
                           value: '10')

  # DR, Al, Gold, 10 mm
  ProductParameter.create!(product: products[:DR_Al_Z_10],
                           parameter: parameters[:profile_type],
                           value: 'DR')
  ProductParameter.create!(product: products[:DR_Al_Z_10],
                           parameter: parameters[:length],
                           value: '3')
  ProductParameter.create!(product: products[:DR_Al_Z_10],
                           parameter: parameters[:height],
                           value: '18')
  ProductParameter.create!(product: products[:DR_Al_Z_10],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:DR_Al_Z_10],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:DR_Al_Z_10],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:DR_Al_Z_10],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:DR_Al_Z_10],
                           parameter: parameters[:groove],
                           value: '10')

  # VRO, Al, Silver, 16 mm
  ProductParameter.create!(product: products[:VRO_Al_S_16],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VRO_Al_S_16],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VRO_Al_S_16],
                           parameter: parameters[:height],
                           value: '43')
  ProductParameter.create!(product: products[:VRO_Al_S_16],
                           parameter: parameters[:width],
                           value: '30')
  ProductParameter.create!(product: products[:VRO_Al_S_16],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:VRO_Al_S_16],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:VRO_Al_S_16],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VRO_Al_S_16],
                           parameter: parameters[:groove],
                           value: '16')

  # HRH, Al, Silver, 16 mm
  ProductParameter.create!(product: products[:HRH_Al_S_16],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HRH_Al_S_16],
                           parameter: parameters[:length],
                           value: '3')
  ProductParameter.create!(product: products[:HRH_Al_S_16],
                           parameter: parameters[:height],
                           value: '78')
  ProductParameter.create!(product: products[:HRH_Al_S_16],
                           parameter: parameters[:width],
                           value: '19')
  ProductParameter.create!(product: products[:HRH_Al_S_16],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:HRH_Al_S_16],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:HRH_Al_S_16],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HRH_Al_S_16],
                           parameter: parameters[:groove],
                           value: '16')

  # HRS, Al, Silver, 16 mm
  ProductParameter.create!(product: products[:HRS_Al_S_16],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HRS_Al_S_16],
                           parameter: parameters[:length],
                           value: '3')
  ProductParameter.create!(product: products[:HRS_Al_S_16],
                           parameter: parameters[:height],
                           value: '43')
  ProductParameter.create!(product: products[:HRS_Al_S_16],
                           parameter: parameters[:width],
                           value: '19')
  ProductParameter.create!(product: products[:HRS_Al_S_16],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:HRS_Al_S_16],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:HRS_Al_S_16],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HRS_Al_S_16],
                           parameter: parameters[:groove],
                           value: '16')

  # DR, Al, Silver, 16 mm
  ProductParameter.create!(product: products[:DR_Al_S_16],
                           parameter: parameters[:profile_type],
                           value: 'DR')
  ProductParameter.create!(product: products[:DR_Al_S_16],
                           parameter: parameters[:length],
                           value: '3')
  ProductParameter.create!(product: products[:DR_Al_S_16],
                           parameter: parameters[:height],
                           value: '18')
  ProductParameter.create!(product: products[:DR_Al_S_16],
                           parameter: parameters[:width],
                           value: '19')
  ProductParameter.create!(product: products[:DR_Al_S_16],
                           parameter: parameters[:color],
                           value: 'Stříbrná')
  ProductParameter.create!(product: products[:DR_Al_S_16],
                           parameter: parameters[:material],
                           value: 'Hliník')
  ProductParameter.create!(product: products[:DR_Al_S_16],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:DR_Al_S_16],
                           parameter: parameters[:groove],
                           value: '16')

  # FE =========================================================================
  # HK, Fe, Gold
  ProductParameter.create!(product: products[:HK_Fe_Z],
                           parameter: parameters[:profile_type],
                           value: 'HK')
  ProductParameter.create!(product: products[:HK_Fe_Z],
                           parameter: parameters[:length],
                           value: '5')
  ProductParameter.create!(product: products[:HK_Fe_Z],
                           parameter: parameters[:height],
                           value: '49')
  ProductParameter.create!(product: products[:HK_Fe_Z],
                           parameter: parameters[:width],
                           value: '109')
  ProductParameter.create!(product: products[:HK_Fe_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:HK_Fe_Z],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:HK_Fe_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HK_Fe_Z],
                           parameter: parameters[:groove],
                           value: '10')
  ProductParameter.create!(product: products[:HK_Fe_Z],
                           parameter: parameters[:groove],
                           value: '4')

  # SK, Fe, Gold
  ProductParameter.create!(product: products[:SK_Fe_Z],
                           parameter: parameters[:profile_type],
                           value: 'SK')
  ProductParameter.create!(product: products[:SK_Fe_Z],
                           parameter: parameters[:length],
                           value: '5')
  ProductParameter.create!(product: products[:SK_Fe_Z],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:SK_Fe_Z],
                           parameter: parameters[:width],
                           value: '104')
  ProductParameter.create!(product: products[:SK_Fe_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:SK_Fe_Z],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:SK_Fe_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:SK_Fe_Z],
                           parameter: parameters[:groove],
                           value: '10')
  ProductParameter.create!(product: products[:SK_Fe_Z],
                           parameter: parameters[:groove],
                           value: '4')

  # VR10, Fe, Gold
  ProductParameter.create!(product: products[:VR10_Fe_Z],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR10_Fe_Z],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR10_Fe_Z],
                           parameter: parameters[:height],
                           value: '32')
  ProductParameter.create!(product: products[:VR10_Fe_Z],
                           parameter: parameters[:width],
                           value: '31')
  ProductParameter.create!(product: products[:VR10_Fe_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:VR10_Fe_Z],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR10_Fe_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VR10_Fe_Z],
                           parameter: parameters[:groove],
                           value: '10')

  # VR10 Lux, Fe, Gold
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Z],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Z],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Z],
                           parameter: parameters[:height],
                           value: '36')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Z],
                           parameter: parameters[:width],
                           value: '32')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Z],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Z],
                           parameter: parameters[:groove],
                           value: '10')

  # VR4, Fe, Gold
  ProductParameter.create!(product: products[:VR4_Fe_Z],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR4_Fe_Z],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR4_Fe_Z],
                           parameter: parameters[:height],
                           value: '32')
  ProductParameter.create!(product: products[:VR4_Fe_Z],
                           parameter: parameters[:width],
                           value: '31')
  ProductParameter.create!(product: products[:VR4_Fe_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:VR4_Fe_Z],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR4_Fe_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VR4_Fe_Z],
                           parameter: parameters[:groove],
                           value: '4')

  # VR4 Lux, Fe, Gold
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Z],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Z],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Z],
                           parameter: parameters[:height],
                           value: '36')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Z],
                           parameter: parameters[:width],
                           value: '32')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Z],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Z],
                           parameter: parameters[:groove],
                           value: '4')

  # HR10, Fe, Gold
  ProductParameter.create!(product: products[:HR10_Fe_Z],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HR10_Fe_Z],
                           parameter: parameters[:length],
                           value: '3,6')
  ProductParameter.create!(product: products[:HR10_Fe_Z],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:HR10_Fe_Z],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:HR10_Fe_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:HR10_Fe_Z],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:HR10_Fe_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HR10_Fe_Z],
                           parameter: parameters[:groove],
                           value: '10')

  # HR4, Fe, Gold
  ProductParameter.create!(product: products[:HR4_Fe_Z],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HR4_Fe_Z],
                           parameter: parameters[:length],
                           value: '3,6')
  ProductParameter.create!(product: products[:HR4_Fe_Z],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:HR4_Fe_Z],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:HR4_Fe_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:HR4_Fe_Z],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:HR4_Fe_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HR4_Fe_Z],
                           parameter: parameters[:groove],
                           value: '4')

  # DR10, Fe, Gold
  ProductParameter.create!(product: products[:DR10_Fe_Z],
                           parameter: parameters[:profile_type],
                           value: 'DR')
  ProductParameter.create!(product: products[:DR10_Fe_Z],
                           parameter: parameters[:length],
                           value: '3,6')
  ProductParameter.create!(product: products[:DR10_Fe_Z],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:DR10_Fe_Z],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:DR10_Fe_Z],
                           parameter: parameters[:color],
                           value: 'Zlatá')
  ProductParameter.create!(product: products[:DR10_Fe_Z],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:DR10_Fe_Z],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:DR10_Fe_Z],
                           parameter: parameters[:groove],
                           value: '10')

  # HK, Fe, White
  ProductParameter.create!(product: products[:HK_Fe_B],
                           parameter: parameters[:profile_type],
                           value: 'HK')
  ProductParameter.create!(product: products[:HK_Fe_B],
                           parameter: parameters[:length],
                           value: '5')
  ProductParameter.create!(product: products[:HK_Fe_B],
                           parameter: parameters[:height],
                           value: '49')
  ProductParameter.create!(product: products[:HK_Fe_B],
                           parameter: parameters[:width],
                           value: '109')
  ProductParameter.create!(product: products[:HK_Fe_B],
                           parameter: parameters[:color],
                           value: 'Bílá')
  ProductParameter.create!(product: products[:HK_Fe_B],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:HK_Fe_B],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HK_Fe_B],
                           parameter: parameters[:groove],
                           value: '10')
  ProductParameter.create!(product: products[:HK_Fe_B],
                           parameter: parameters[:groove],
                           value: '4')

  # SK, Fe, White
  ProductParameter.create!(product: products[:SK_Fe_B],
                           parameter: parameters[:profile_type],
                           value: 'SK')
  ProductParameter.create!(product: products[:SK_Fe_B],
                           parameter: parameters[:length],
                           value: '5')
  ProductParameter.create!(product: products[:SK_Fe_B],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:SK_Fe_B],
                           parameter: parameters[:width],
                           value: '104')
  ProductParameter.create!(product: products[:SK_Fe_B],
                           parameter: parameters[:color],
                           value: 'Bílá')
  ProductParameter.create!(product: products[:SK_Fe_B],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:SK_Fe_B],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:SK_Fe_B],
                           parameter: parameters[:groove],
                           value: '10')
  ProductParameter.create!(product: products[:SK_Fe_B],
                           parameter: parameters[:groove],
                           value: '4')

  # VR10, Fe, White
  ProductParameter.create!(product: products[:VR10_Fe_B],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR10_Fe_B],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR10_Fe_B],
                           parameter: parameters[:height],
                           value: '32')
  ProductParameter.create!(product: products[:VR10_Fe_B],
                           parameter: parameters[:width],
                           value: '31')
  ProductParameter.create!(product: products[:VR10_Fe_B],
                           parameter: parameters[:color],
                           value: 'Bílá')
  ProductParameter.create!(product: products[:VR10_Fe_B],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR10_Fe_B],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VR10_Fe_B],
                           parameter: parameters[:groove],
                           value: '10')

  # VR10 Lux, Fe, White
  ProductParameter.create!(product: products[:VR10_Lux_Fe_B],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_B],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_B],
                           parameter: parameters[:height],
                           value: '36')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_B],
                           parameter: parameters[:width],
                           value: '32')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_B],
                           parameter: parameters[:color],
                           value: 'Bílá')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_B],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_B],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_B],
                           parameter: parameters[:groove],
                           value: '10')

  # VR4, Fe, White
  ProductParameter.create!(product: products[:VR4_Fe_B],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR4_Fe_B],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR4_Fe_B],
                           parameter: parameters[:height],
                           value: '32')
  ProductParameter.create!(product: products[:VR4_Fe_B],
                           parameter: parameters[:width],
                           value: '31')
  ProductParameter.create!(product: products[:VR4_Fe_B],
                           parameter: parameters[:color],
                           value: 'Bílá')
  ProductParameter.create!(product: products[:VR4_Fe_B],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR4_Fe_B],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VR4_Fe_B],
                           parameter: parameters[:groove],
                           value: '4')

  # VR4 Lux, Fe, White
  ProductParameter.create!(product: products[:VR4_Lux_Fe_B],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_B],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_B],
                           parameter: parameters[:height],
                           value: '36')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_B],
                           parameter: parameters[:width],
                           value: '32')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_B],
                           parameter: parameters[:color],
                           value: 'Bílá')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_B],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_B],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_B],
                           parameter: parameters[:groove],
                           value: '4')

  # HR10, Fe, White
  ProductParameter.create!(product: products[:HR10_Fe_B],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HR10_Fe_B],
                           parameter: parameters[:length],
                           value: '3,6')
  ProductParameter.create!(product: products[:HR10_Fe_B],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:HR10_Fe_B],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:HR10_Fe_B],
                           parameter: parameters[:color],
                           value: 'Bílá')
  ProductParameter.create!(product: products[:HR10_Fe_B],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:HR10_Fe_B],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HR10_Fe_B],
                           parameter: parameters[:groove],
                           value: '10')

  # HR4, Fe, White
  ProductParameter.create!(product: products[:HR4_Fe_B],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HR4_Fe_B],
                           parameter: parameters[:length],
                           value: '3,6')
  ProductParameter.create!(product: products[:HR4_Fe_B],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:HR4_Fe_B],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:HR4_Fe_B],
                           parameter: parameters[:color],
                           value: 'Bílá')
  ProductParameter.create!(product: products[:HR4_Fe_B],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:HR4_Fe_B],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:HR4_Fe_B],
                           parameter: parameters[:groove],
                           value: '4')

  # DR10, Fe, White
  ProductParameter.create!(product: products[:DR10_Fe_B],
                           parameter: parameters[:profile_type],
                           value: 'DR')
  ProductParameter.create!(product: products[:DR10_Fe_B],
                           parameter: parameters[:length],
                           value: '3,6')
  ProductParameter.create!(product: products[:DR10_Fe_B],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:DR10_Fe_B],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:DR10_Fe_B],
                           parameter: parameters[:color],
                           value: 'Bílá')
  ProductParameter.create!(product: products[:DR10_Fe_B],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:DR10_Fe_B],
                           parameter: parameters[:color_type],
                           value: 'Základní barva')
  ProductParameter.create!(product: products[:DR10_Fe_B],
                           parameter: parameters[:groove],
                           value: '10')

  # HK, Fe, Maple
  ProductParameter.create!(product: products[:HK_Fe_Ja],
                           parameter: parameters[:profile_type],
                           value: 'HK')
  ProductParameter.create!(product: products[:HK_Fe_Ja],
                           parameter: parameters[:length],
                           value: '5')
  ProductParameter.create!(product: products[:HK_Fe_Ja],
                           parameter: parameters[:height],
                           value: '49')
  ProductParameter.create!(product: products[:HK_Fe_Ja],
                           parameter: parameters[:width],
                           value: '109')
  ProductParameter.create!(product: products[:HK_Fe_Ja],
                           parameter: parameters[:color],
                           value: 'Javor')
  ProductParameter.create!(product: products[:HK_Fe_Ja],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:HK_Fe_Ja],
                           parameter: parameters[:color_type],
                           value: 'Dřevodekor')
  ProductParameter.create!(product: products[:HK_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '10')
  ProductParameter.create!(product: products[:HK_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '4')

  # SK, Fe, Maple
  ProductParameter.create!(product: products[:SK_Fe_Ja],
                           parameter: parameters[:profile_type],
                           value: 'SK')
  ProductParameter.create!(product: products[:SK_Fe_Ja],
                           parameter: parameters[:length],
                           value: '5')
  ProductParameter.create!(product: products[:SK_Fe_Ja],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:SK_Fe_Ja],
                           parameter: parameters[:width],
                           value: '104')
  ProductParameter.create!(product: products[:SK_Fe_Ja],
                           parameter: parameters[:color],
                           value: 'Javor')
  ProductParameter.create!(product: products[:SK_Fe_Ja],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:SK_Fe_Ja],
                           parameter: parameters[:color_type],
                           value: 'Dřevodekor')
  ProductParameter.create!(product: products[:SK_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '10')
  ProductParameter.create!(product: products[:SK_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '4')

  # VR10, Fe, Maple
  ProductParameter.create!(product: products[:VR10_Fe_Ja],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR10_Fe_Ja],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR10_Fe_Ja],
                           parameter: parameters[:height],
                           value: '32')
  ProductParameter.create!(product: products[:VR10_Fe_Ja],
                           parameter: parameters[:width],
                           value: '31')
  ProductParameter.create!(product: products[:VR10_Fe_Ja],
                           parameter: parameters[:color],
                           value: 'Javor')
  ProductParameter.create!(product: products[:VR10_Fe_Ja],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR10_Fe_Ja],
                           parameter: parameters[:color_type],
                           value: 'Dřevodekor')
  ProductParameter.create!(product: products[:VR10_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '10')

  # VR10 Lux, Fe, Maple
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Ja],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Ja],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Ja],
                           parameter: parameters[:height],
                           value: '36')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Ja],
                           parameter: parameters[:width],
                           value: '32')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Ja],
                           parameter: parameters[:color],
                           value: 'Javor')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Ja],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Ja],
                           parameter: parameters[:color_type],
                           value: 'Dřevodekor')
  ProductParameter.create!(product: products[:VR10_Lux_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '10')

  # VR4, Fe, Maple
  ProductParameter.create!(product: products[:VR4_Fe_Ja],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR4_Fe_Ja],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR4_Fe_Ja],
                           parameter: parameters[:height],
                           value: '32')
  ProductParameter.create!(product: products[:VR4_Fe_Ja],
                           parameter: parameters[:width],
                           value: '31')
  ProductParameter.create!(product: products[:VR4_Fe_Ja],
                           parameter: parameters[:color],
                           value: 'Javor')
  ProductParameter.create!(product: products[:VR4_Fe_Ja],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR4_Fe_Ja],
                           parameter: parameters[:color_type],
                           value: 'Dřevodekor')
  ProductParameter.create!(product: products[:VR4_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '4')

  # VR4 Lux, Fe, Maple
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Ja],
                           parameter: parameters[:profile_type],
                           value: 'VR')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Ja],
                           parameter: parameters[:length],
                           value: '2,75')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Ja],
                           parameter: parameters[:height],
                           value: '36')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Ja],
                           parameter: parameters[:width],
                           value: '32')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Ja],
                           parameter: parameters[:color],
                           value: 'Javor')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Ja],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Ja],
                           parameter: parameters[:color_type],
                           value: 'Dřevodekor')
  ProductParameter.create!(product: products[:VR4_Lux_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '4')

  # HR10, Fe, Maple
  ProductParameter.create!(product: products[:HR10_Fe_Ja],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HR10_Fe_Ja],
                           parameter: parameters[:length],
                           value: '3,6')
  ProductParameter.create!(product: products[:HR10_Fe_Ja],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:HR10_Fe_Ja],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:HR10_Fe_Ja],
                           parameter: parameters[:color],
                           value: 'Javor')
  ProductParameter.create!(product: products[:HR10_Fe_Ja],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:HR10_Fe_Ja],
                           parameter: parameters[:color_type],
                           value: 'Dřevodekor')
  ProductParameter.create!(product: products[:HR10_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '10')

  # HR4, Fe, Maple
  ProductParameter.create!(product: products[:HR4_Fe_Ja],
                           parameter: parameters[:profile_type],
                           value: 'HR')
  ProductParameter.create!(product: products[:HR4_Fe_Ja],
                           parameter: parameters[:length],
                           value: '3,6')
  ProductParameter.create!(product: products[:HR4_Fe_Ja],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:HR4_Fe_Ja],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:HR4_Fe_Ja],
                           parameter: parameters[:color],
                           value: 'Javor')
  ProductParameter.create!(product: products[:HR4_Fe_Ja],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:HR4_Fe_Ja],
                           parameter: parameters[:color_type],
                           value: 'Dřevodekor')
  ProductParameter.create!(product: products[:HR4_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '4')

  # DR10, Fe, Maple
  ProductParameter.create!(product: products[:DR10_Fe_Ja],
                           parameter: parameters[:profile_type],
                           value: 'DR')
  ProductParameter.create!(product: products[:DR10_Fe_Ja],
                           parameter: parameters[:length],
                           value: '3,6')
  ProductParameter.create!(product: products[:DR10_Fe_Ja],
                           parameter: parameters[:height],
                           value: '13')
  ProductParameter.create!(product: products[:DR10_Fe_Ja],
                           parameter: parameters[:width],
                           value: '13')
  ProductParameter.create!(product: products[:DR10_Fe_Ja],
                           parameter: parameters[:color],
                           value: 'Javor')
  ProductParameter.create!(product: products[:DR10_Fe_Ja],
                           parameter: parameters[:material],
                           value: 'Ocel')
  ProductParameter.create!(product: products[:DR10_Fe_Ja],
                           parameter: parameters[:color_type],
                           value: 'Dřevodekor')
  ProductParameter.create!(product: products[:DR10_Fe_Ja],
                           parameter: parameters[:groove],
                           value: '10')
end
