# frozen_string_literal: true

# Seed Addresses
def seed_addresses(contacts, companies)
  addresses = []
  addresses[0] = Address.create!(
    street: 'Zelená',
    number: '23',
    city: 'Praha 4',
    zip: '14200',
    country: 'Česká Republika',
    contact: contacts[0]
  )
  addresses[1] = Address.create!(
    street: 'Náměstí generála Kutlvašra',
    number: '23/1423',
    city: 'Praha 4',
    zip: '14000',
    country: 'Česká Republika',
    contact: contacts[0]
  )
  addresses[2] = Address.create!(
    street: 'Václavské náměstí',
    number: '124a/12',
    city: 'Praha 1',
    zip: '11000',
    country: 'Česká Republika',
    contact: contacts[1],
    company: companies[0]
  )
  addresses[3] = Address.create!(
    street: 'Pražská',
    number: '734',
    city: 'Líbeznice',
    zip: '87223',
    country: 'Česká Republika',
    contact: contacts[2]
  )
  addresses[4] = Address.create!(
    street: 'Věloposlká',
    number: '1',
    city: 'Roztoky u Prahy',
    zip: '33509',
    country: 'Česká Republika',
    company: companies[2]
  )
  addresses[5] = Address.create!(
    street: 'Václava Klementa',
    number: '2322/12',
    city: 'Mladá Boleslav - Čejetice',
    zip: '83412',
    country: 'Česká Republika',
    contact: contacts[3],
  )
  addresses[6] = Address.create!(
    street: 'Zelenohorská',
    number: '33',
    city: 'Mladá Boleslav',
    zip: '83414',
    country: 'Česká Republika',
    company: companies[1]
  )
  addresses[7] = Address.create!(
    street: 'Sluneční',
    number: '99',
    city: 'Říčany',
    zip: '98933',
    country: 'Česká Republika',
    contact: contacts[4]
  )

  return addresses
end
