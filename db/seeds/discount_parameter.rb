# frozen_string_literal: true

# Seed Discount categories
def seed_discount_parameters(discounts, parameters)
  DiscountParameter.create!(discount: discounts[0],
                            parameter: parameters[:color],
                            value: 'Zlatá')
  DiscountParameter.create!(discount: discounts[0],
                            parameter: parameters[:material],
                            value: 'Ocel')
  DiscountParameter.create!(discount: discounts[1],
                            parameter: parameters[:material],
                            value: 'Hliník')
  DiscountParameter.create!(discount: discounts[1],
                            parameter: parameters[:color],
                            value: 'Stříbrná')
  DiscountParameter.create!(discount: discounts[3],
                            parameter: parameters[:profile_type],
                            value: 'HK')
  DiscountParameter.create!(discount: discounts[4],
                            parameter: parameters[:material],
                            value: 'Ocel')
  DiscountParameter.create!(discount: discounts[5],
                            parameter: parameters[:color_type],
                            value: 'Dřevodekor')
end
