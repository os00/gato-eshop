# frozen_string_literal: true

# Seed Categories
def seed_categories
  categories = {}

  # Products type categories
  categories[:product_type] =
    Category.create!(name: 'Profily',
                     description: 'Rozdělení dle typů výrobků (Al, Fe...)',
                     parent_category: 0)
  categories[:fe] =
    Category.create!(name: 'Ocelový profil',
                     description: 'Ocelové profily',
                     parent_category: categories[:product_type].id)
  categories[:al] =
    Category.create!(name: 'Hliníkový profil',
                     description: 'Hliníkové profily',
                     parent_category: categories[:product_type].id)
  categories[:additional_fe] =
    Category.create!(name: 'Doplňkový materiál ocel',
                     description: 'Další materiál k ocelovým profilům (pojezdy, kartáčky apod.)',
                     parent_category: categories[:product_type].id)
  categories[:additional_al] =
    Category.create!(name: 'Doplňkový materiál hliník',
                     description: 'Další materiál k hliníkovým profilům (pojezdy, kartáčky apod.)',
                     parent_category: categories[:product_type].id)

  # Further subcategories
  categories[:basic_color] =
    Category.create!(name: 'Základní barvy',
                     description: 'Základní barvy (bez dřevodekoru)',
                     parent_category: categories[:fe].id)
  categories[:wooden_decor] =
    Category.create!(name: 'Dřevodekory',
                     description: 'Dřevěné a jiné dekory',
                     parent_category: categories[:fe].id)

  # Possibly for later use
  # categories[:color] =
  #   Category.create!(name: 'Barva',
  #                    description: 'Rozdělení dle barvy výrobků',
  #                    parent_category: 0)
  # categories[:basic_colors] =
  #   Category.create!(name: 'Základní barvy',
  #                    description: 'Základní barvy (bez dřevodekoru)',
  #                    parent_category: categories[:color].id)
  # categories[:silver] =
  #   Category.create!(name: 'Stříbrná',
  #                    description: 'Stříbrné profily',
  #                    parent_category: categories[:basic_colors].id)
  # categories[:golden] =
  #   Category.create!(name: 'Zlatá',
  #                    description: 'Zlaté profily',
  #                    parent_category: categories[:basic_colors].id)
  # categories[:white] =
  #   Category.create!(name: 'Bílá',
  #                    description: 'Bílé profily',
  #                    parent_category: categories[:basic_colors].id)
  # categories[:wooden_decors] =
  #   Category.create!(name: 'Dřevodekory',
  #                    description: 'Dřevěné a jiné dekory',
  #                    parent_category: categories[:color].id)
  # categories[:cherry] =
  #   Category.create!(name: 'Třešeň',
  #                    description: 'Profily s dřevodekorem třešeň',
  #                    parent_category: categories[:wooden_decors].id)
  # categories[:maple] =
  #   Category.create!(name: 'Javor',
  #                    description: 'Profily s dřevodekorem javor',
  #                    parent_category: categories[:wooden_decors].id)

  return categories
end
