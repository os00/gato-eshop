# frozen_string_literal: true

# Seed Payments
def seed_payments(orders)
  Payment.create!(
    price: 0,
    number: "P#{orders[0].number}",
    status: 'paid',
    method: 'cc',
    order: orders[0]
  )
  Payment.create!(
    price: 0,
    number: "P#{orders[1].number}",
    status: 'not_paid',
    method: 'cash',
    order: orders[1]
  )
  Payment.create!(
    price: 0,
    number: "P#{orders[2].number}",
    status: 'not_paid',
    method: 'cash',
    order: orders[2]
  )
  Payment.create!(
    price: 79,
    number: "P#{orders[3].number}",
    status: 'not_paid',
    method: 'cod',
    order: orders[3]
  )
  Payment.create!(
    price: 99,
    number: "P#{orders[4].number}",
    status: 'not_paid',
    method: 'cod',
    order: orders[4]
  )
  Payment.create!(
    price: 0,
    number: "P#{orders[5].number}",
    status: 'processing',
    method: 'transfer',
    order: orders[5]
  )
  Payment.create!(
    price: 79,
    number: "P#{orders[6].number}",
    status: 'not_paid',
    method: 'cod',
    order: orders[6]
  )
  Payment.create!(
    price: 0,
    number: "P#{orders[7].number}",
    status: 'paid',
    method: 'cc',
    order: orders[7]
  )
  Payment.create!(
    price: 0,
    number: "P#{orders[8].number}",
    status: 'paid',
    method: 'cc',
    order: orders[8]
  )
  Payment.create!(
    price: 0,
    number: "P#{orders[9].number}",
    status: 'paid',
    method: 'cash',
    order: orders[9]
  )
  Payment.create!(
    price: 0,
    number: "P#{orders[10].number}",
    status: 'paid',
    method: 'transfer',
    order: orders[10]
  )
end
