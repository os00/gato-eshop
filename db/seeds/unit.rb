# frozen_string_literal: true

# Seed Units
def seed_units
  units = {}
  units[:ks] = Unit.create!(name: 'ks')
  units[:m] = Unit.create!(name: 'm')
  units[:mm] = Unit.create!(name: 'mm')

  return units
end
