# frozen_string_literal: true

# Seed Discounts
def seed_discounts
  date_now = Date.today
  discounts = []
  discounts[0] = Discount.create!(
    is_active: true, description: 'General discount',
    start_date: date_now, end_date: date_now + 365,
    percent_value: 10
  )
  discounts[1] = Discount.create!(
    is_active: true, description: 'General discount',
    start_date: date_now - 100, end_date: date_now - 1,
    percent_value: 24
  )
  discounts[2] = Discount.create!(
    is_active: true, description: 'General discount',
    turnover_value: 5000, turnover_period: 365,
    percent_value: 5
  )
  discounts[3] = Discount.create!(
    is_active: true, description: 'General discount',
    start_date: date_now, end_date: date_now + 365,
    turnover_value: 5000, turnover_period: 365,
    percent_value: 50
  )
  discounts[4] = Discount.create!(
    is_active: false, description: 'General discount',
    start_date: date_now, end_date: date_now + 365,
    percent_value: 3
  )
  discounts[5] = Discount.create!(
    is_active: false, description: 'General discount',
    turnover_value: 10_000, turnover_period: 180,
    percent_value: 33
  )

  return discounts
end
