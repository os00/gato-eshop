#!/bin/sh
cd /Users/os00/code/os00/gato-eshop/app/assets/images/products

for i in {0..44}
do
  mkdir $i
  cd $i
  remainder=`expr $i % 3`
  echo $remainder
  if [ "$remainder" -eq "0" ]
  then
    cp /Users/os00/documents/temp/P01-01.jpg ./P01-01.jpg
    cp /Users/os00/documents/temp/P01-02.jpg ./P01-02.jpg
    cp /Users/os00/documents/temp/P01-03.jpg ./P01-03.jpg
  fi
  if [ "$remainder" -eq "1" ]
  then
    cp /Users/os00/documents/temp/P02-01.jpg ./P02-01.jpg
    cp /Users/os00/documents/temp/P02-02.jpg ./P02-02.jpg
    cp /Users/os00/documents/temp/P02-03.jpg ./P02-03.jpg
  fi
  if [ "$remainder" -eq "2" ]
  then
    cp /Users/os00/documents/temp/P03-01.jpg ./P03-01.jpg
    cp /Users/os00/documents/temp/P03-02.jpg ./P03-02.jpg
    cp /Users/os00/documents/temp/P03-03.jpg ./P03-03.jpg
  fi
  cd ..
done
